# human-resources
Sistema de Recursos Humanos.

### Como rodar o projeto

- Clone o projeto
- Abra o git na pasta do projeto clonado
- Rode o comando "npm i" para instalar as dependências.
- Rode o comando "npm start".
- Abra a pagina localhost/9000 no seu navegador
