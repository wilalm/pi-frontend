const express = require("express");
const path = require("path");

const app = express();

app.use("/static", express.static(path.resolve(__dirname, "src", "static")));

app.get("/", (req, res) => {
    res.sendFile(path.resolve(__dirname, "src", "login.html"));
});

app.get("/register", (req, res) => {
    res.sendFile(path.resolve(__dirname, "src", "register.html"));
});

app.get("/*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "src", "index.html"));
});

app.listen(process.env.PORT || 9000, () => console.log("Server running..."));