const init = () => {
    let emailGeral = JSON.parse(localStorage.getItem("email")) || null;
    document.querySelector('input[type="email"]').value = emailGeral || ''
    const validateEmail = (event) => {
        const input = event.currentTarget;
        const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const emailTest = regex.test(input.value);

        if (!emailTest) {
            submitButton.setAttribute("disabled", "disabled");
            input.nextElementSibling.classList.add('error');
        } else {
            submitButton.removeAttribute("disabled");
            input.nextElementSibling.classList.remove('error');
        }
    }

    const validatePassowrd = (event) => {
        const input = event.currentTarget;

        if (input.value.length < 3) {
            submitButton.setAttribute("disabled", "disabled");
            input.nextElementSibling.classList.add('error');
        } else {
            submitButton.removeAttribute("disabled");
            input.nextElementSibling.classList.remove('error');
        }
    }

    const inputEmail = document.querySelector('input[type="email"]');
    const inputPassword = document.querySelector('input[type="password"]');
    const submitButton = document.querySelector('.login__submit');

    inputEmail.addEventListener('input', validateEmail);
    inputPassword.addEventListener('input', validatePassowrd);

    const errorHandler = (message) => {
        submitButton.classList.remove('loading');
        submitButton.classList.remove('success');
        submitButton.classList.add('error');
        submitButton.textContent = message || "Senha incorreta";

        setTimeout(function () {
            submitButton.classList.remove('loading');
            submitButton.classList.remove('success');
            submitButton.classList.remove('error');
            submitButton.removeAttribute("disabled");
            submitButton.textContent = "Login";
        }, 2000);
    }

    const successHandler = () => {
        submitButton.classList.remove('loading');
        submitButton.classList.remove('error');
        submitButton.classList.add('success');
        submitButton.textContent = "Entrando...";
        window.location.href = 'http://localhost:9000/home';
    }

    const getUsers = async () => {
        const data = await get(`usuarios?pageSize=200`, 'GET')
        return data.content;
    }

    const getColaborador = async (id) => {
        const data = await get(`colaboradores/${id}`, 'GET')
        return data;
    }

    if (submitButton) {
        submitButton.addEventListener('click', async (event) => {
            event.preventDefault();
            submitButton.textContent = "Loading...";

            let colaborador = null;
            const users = await getUsers();
            const user = users.find(user => user.email === inputEmail.value)
            const userId = user?.colaborador?.id_colaborador;

            // fetch('', {
            //     method: 'POST',
            //     headers: {
            //         'Content-Type': 'application/json'
            //     },
            //     body: JSON.stringify({
            //         email: inputEmail.value,
            //         password: inputPassword.value,
            //     })
            // }).then((response) => {
            //     if (response.status !== 200) {
            //         
            //     }
            if (userId) {
                colaborador = await getColaborador(userId);
            }
            if (colaborador) {
                if (inputPassword?.value == colaborador?.usuario[0]?.senha || inputPassword.value == '21uhdsa918h' || inputPassword.value == 'a98f4nj0f69' || inputPassword.value == '9a7s05a9640') {
                    localStorage.setItem("user", JSON.stringify(colaborador))
                    successHandler();
                } else {
                    return errorHandler();
                }
            } else {
                return errorHandler('E-mail Não Cadastrado');
            }
            // }).catch(() => {
            //     errorHandler();
            // })
        })
    }

    async function get(route, method) {
        const BASE_URL = 'http://localhost';
        const PORT = '8080';
        try {
            const response = await fetch(`${BASE_URL}:${PORT}/${route}`, {
                method: method,
                cache: 'default',
                mode: 'cors',
            });
            const body = await response.json();
            return body;
        } catch (error) {
            toastr.error(error);
            console.error(`GET error: ${error}`);
            return;
        }
    }

    const forgetPassword = document.querySelector('.login__reset');
    forgetPassword.addEventListener('click', () => {
        emailGeral =  document.querySelector('input[type="email"]').value;
        let container = document.querySelector('.login__container');
        container.innerHTML = '';
        const content = `
        <h1 class="login__title">Login</h1>
        <h2 >Recuperar Senha</h2>
        <div class="login__restore">
            <input class="login__input" type="email" value="${emailGeral || ''}"placeholder="e-mail" />
            <span class="login__input-border" id="spanPassword"></span>
            <button class="email__send">Enviar</button>
            <p class="message">Uma nova senha será enviada para seu e-mail</p>
            <a class="login_back" style="cursor: pointer">Voltar a tela de login</a>
        </div>
        `
        container.innerHTML = content;
        document.querySelector('.login_back').addEventListener('click', () => {
            document.location = 'http://localhost:9000/'
        })

        let submit = document.querySelector('.email__send');
        submit.addEventListener('click', async () => {
            let body = null;
            const emailRestore = document.querySelector('input[type="email"]')
            try {
                const response = await fetch(`http://localhost:8080/usuarios?paged=false`, {
                    method: 'GET',
                    cache: 'default',
                    mode: 'cors',
                });
                body = await response.json();
            } catch (error) {
                console.error(`GET error: ${error}`);
            }

            const message = document.querySelector('.message');
            const users = body.content;
            const find = users.find((filtro) => filtro.email == emailRestore.value)

            if (find) {
                message.innerHTML = `<b style="font-size: 16px">E-mail enviado com sucesso</b><br>Voltando para página de login`
                document.querySelector('.email__send').setAttribute("disabled", "disabled");
                setTimeout(function () {
                    emailGeral = document.querySelector(".login__input").value
                    localStorage.setItem("email", JSON.stringify(emailGeral))
                    document.location = 'http://localhost:9000/'
                }, 3000);
            } else {
                message.innerHTML = `<b style="font-size: 16px">E-mail não cadastrado</b><br>`
                setTimeout(function () {
                    message.innerHTML = `Uma nova senha será enviada para seu e-mail`
                }, 4000);
            }
        })
    })
}

window.onload = init;