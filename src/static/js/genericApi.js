
const BASE_URL = 'http://localhost';
const PORT = '8080';

export default async function genericApi(route, method, params) {
    switch (method) {
        case 'GET': {
            try {
                const response = await fetch(`${BASE_URL}:${PORT}/${route}`, {
                    method: method,
                    cache: 'default',
                    mode: 'cors',
                });
                const body = await response.json();
                return body;
            } catch (error) {
                toastr.error(error);
                console.error(`GET error: ${error}`);
                return;
            }
        };
        case 'DELETE': {
            try {
                const response = await fetch(`${BASE_URL}:${PORT}/${route}`, {
                    method: method,
                    cache: 'default',
                    mode: 'cors',
                });
                if (response.status >= 200 && response.status < 300 ) {
                    toastr.sucessDelete();
                } else {
                    const body = await response.json();
                    toastr.error(body.user_message);
                }
                return;
            } catch (err) {
                console.log(`GET error: ${err}`);
                return;
            }
        }
        case 'PUT': {
            try {
                const response = await fetch(`${BASE_URL}:${PORT}/${route}`, {
                    method: method,
                    cache: 'default',
                    mode: 'cors',
                    body: JSON.stringify(params),
                    headers: { 'Content-type': 'application/json' },
                });
                if (response.status >= 200 && response.status < 300 ) {
                    toastr.sucessEdit();
                } else {
                    const body = await response.json();
                    toastr.error(body.user_message);
                }
                return;
            } catch (error) {
                console.error(`GET error: ${error}`);
                return;
            }
        }
        case 'POST': {
            try {
                const response = await fetch(`${BASE_URL}:${PORT}/${route}`, {
                    method: method,
                    cache: 'default',
                    mode: 'cors',
                    body: JSON.stringify(params),
                    headers: { 'Content-type': 'application/json' },
                });
                if (response.status >= 200 && response.status < 300 ) {
                    toastr.sucessInclude();
                } else {
                    const body = await response.json();
                    toastr.error(body.user_message);
                }
                return;
            } catch (error) {
                console.error(`GET error: ${error}`);
                return;
            }
        }
    }
}

const toastr = {
    sucessInclude() {
        const toastrModal = document.querySelector(".toastr");
        toastrModal.classList.add('show__toastr');
        const toastrmessage = toastrModal.querySelector(".toastr__message");
        toastrmessage.classList.add('toastr__sucess');
        toastrmessage.innerHTML = `<p><b>Sucesso</b></p>
                                    <p>Operação realizada com sucesso</p>`;
        setTimeout(function(){ 
            toastrModal.classList.remove('show__toastr'); 
            toastrmessage.classList.remove('toastr__sucess');
        }, 4000);
    },

    sucessDelete() {
        const toastrModal = document.querySelector(".toastr");
        toastrModal.classList.add('show__toastr');
        const toastrmessage = toastrModal.querySelector(".toastr__message");
        toastrmessage.classList.add('toastr__sucess');
        toastrmessage.innerHTML = `<b>Sucesso</b>
                                    <p>Sucesso ao deletar!</p>`;
        setTimeout(function(){ 
            toastrModal.classList.remove('show__toastr'); 
            toastrmessage.classList.remove('toastr__sucess');
        }, 4000);
    },

    sucessEdit() {
        const toastrModal = document.querySelector(".toastr");
        toastrModal.classList.add('show__toastr');
        const toastrmessage = toastrModal.querySelector(".toastr__message");
        toastrmessage.classList.add('toastr__sucess');
        toastrmessage.innerHTML = `<b>Sucesso</b>
                                    <p>Operação realizada com sucesso!</p>`;
        setTimeout(function(){ 
            toastrModal.classList.remove('show__toastr'); 
            toastrmessage.classList.remove('toastr__sucess');
        }, 3700);
    },

    error(error) {
        const toastrModal = document.querySelector(".toastr");
        toastrModal.classList.add('show__toastr');
        const toastrmessage = toastrModal.querySelector(".toastr__message");
        toastrmessage.classList.add('toastr__error');
        toastrmessage.innerHTML = `<b>Erro</b>
                                    <p>${error}</p>`;
        setTimeout(function(){ 
            toastrModal.classList.remove('show__toastr'); 
            toastrmessage.classList.remove('toastr__error');
        }, 3700);
    }
}