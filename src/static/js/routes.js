// IMPORTAÇÃO DAS VIEWS
import navSlide from "./views/Menu/Menu.js";
import NotFound from "./views/NotFound/NotFound.js";
import Home from "./views/Home/Home.js";
import Assesments from "./views/Assesments/Assesments.js";
import Carrer from "./views/Carrer/Carrer.js";
import CarrerByPlan from "./views/Carrer/CarrerByPlan.js";
import Departaments from "./views/Departaments/Departaments.js";
import Benefits from "./views/Benefits/Benefits.js";
import Workload  from "./views/Workload/Workload.js";
import WorkloadById  from "./views/Workload/WorkloadById.js";
import Profile from "./views/Profile/Profile.js";
import Employees from "./views/Employees/Employees.js";
import EmployeeById from "./views/Employees/EmployeeById.js";
import Promotions from "./views/Promotions/Promotions.js";
import Relatorios from "./views/Relatorios/Relatorios.js";
import Groups from "./views/Groups/Groups.js";
import Users from "./views/Users/Users.js";
import check from "./views/Permissions/Permissions.js";

const pathToRegex = path => new RegExp("^" + path.replace(/\//g, "\\/").replace(/:\w+/g, "(.+)") + "$");
navSlide();
check();

const getParams = match => {
    const values = match.result.slice(1);
    const keys = Array.from(match.route.path.matchAll(/:(\w+)/g)).map(result => result[1]);

    return Object.fromEntries(keys.map((key, i) => {
        return [key, values[i]];
    }));
};

const navigateTo = url => {
    history.pushState(null, null, url);
    router();
};

const router = async () => {
    const routes = [
        { path: "/notfound",             view: NotFound },
        { path: "/home",                 view: Home },
        { path: "/avaliacoes",           view: Assesments },
        { path: "/beneficios",           view: Benefits },
        { path: "/cargos",               view: Workload },
        { path: "/cargos/:id",           view: WorkloadById},
        { path: "/carreira",             view: Carrer },
        { path: "/carreira/:id",         view: CarrerByPlan },
        { path: "/departamentos",        view: Departaments },
        { path: "/colaboradores",        view: Employees },
        { path: "/colaborador/:id",      view: EmployeeById },
        { path: "/perfil",               view: Profile },
        { path: "/promocoes",            view: Promotions },
        { path: "/relatorios",           view: Relatorios },
        { path: "/permissoes",           view: Groups },
        { path: "/usuarios",             view: Users },
    ];

    const potentialMatches = routes.map(route => {
        return {
            route: route,
            result: location.pathname.match(pathToRegex(route.path))
        };
    });

    let match = potentialMatches.find(potentialMatch => potentialMatch.result !== null);

    if (!match) {
        match = {
            route: routes[0],
            result: [location.pathname]
        };
    }

    const view = new match.route.view(getParams(match));

    document.querySelector("#app").innerHTML = await view.getHtml();
    view.init();
};

window.addEventListener("popstate", router);

document.addEventListener("DOMContentLoaded", () => {
    document.body.addEventListener("click", e => {
        if (e.target.matches("[data-link]")) {
            e.preventDefault();
            navigateTo(e.target.href);
        }
    });

    router();
});