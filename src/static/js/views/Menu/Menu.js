//Menu
export default function navSlide() {
    const iconMenu = document.querySelector(".btn__menu");
    const nav = document.querySelector(".menu");
    const app = document.querySelector(".app");
  
    iconMenu.addEventListener("click", () => {
      nav.classList.toggle("menu-active");
      app.classList.toggle("app-active");
    });
  };

  if(window.screen.width < 620) {
    document.querySelector('.fa-bars').innerHTML = '';
  } else {
    document.querySelector('.fa-bars').innerHTML = ' Menu';
  }
