import genericApi from "../../genericApi.js";

export default async function check() {
    const user = JSON.parse(localStorage.getItem("user"));

    const gerenciados = await getGerenciado(user.id_colaborador);

    const grupo = await getGrupo(user.usuario[0].grupo.id_grupo)

    const permissoes = grupo.permissoes

    const avaliacoesMenu = document.getElementById('avaliacoesMenu');
    const beneficiosMenu = document.getElementById('beneficiosMenu');
    const cargosMenu = document.getElementById('cargosMenu');
    const departamentosMenu = document.getElementById('departamentosMenu');
    const colaboradoresMenu = document.getElementById('colaboradoresMenu');
    const carreiraMenu = document.getElementById('carreiraMenu');
    const promocoesMenu = document.getElementById('promocoesMenu');
    const relatoriosMenu = document.getElementById('relatoriosMenu');
    const permissoesMenu = document.getElementById('permissoesMenu');
    const usuariosMenu = document.getElementById('usuariosMenu');

    permissoes.map((permissao) => {
        switch (permissao.id_permissao) {
            case 1: //Gerenciar Cargos
                cargosMenu.style.display = 'block';
                break;
            case 2: //Gerenciar Departamentos
                departamentosMenu.style.display = 'block'
                break;
            case 3: //Gerenciar Colaboradores
                colaboradoresMenu.style.display = 'block';
                break;
            case 4: //Gerenciar Benefícios
                beneficiosMenu.style.display = 'block';
            break;
            case 5: //Gerenciar Usuários
                usuariosMenu.style.display = 'block';
                break;
            case 6: //Gerenciar Grupos
                permissoesMenu.style.display = 'block';
                break;
            case 7: //Visualizar informações próprias e histórico
                break;
            case 8: //Gerenciar Planos de Carreira
                carreiraMenu.style.display = 'block';
                break;
            case 9: //Promover Colaboradores
                promocoesMenu.style.display = 'block';
                break;
            case 10: //Emitir Relatório - Eventos Colaborador
                relatoriosMenu.style.display = 'block';
                break;
            case 11: //Emitir Relatório - Diversidade - Idade
                relatoriosMenu.style.display = 'block';
                break;
            case 12: //Emitir Relatório - Custo Total por Benefício
                relatoriosMenu.style.display = 'block';
                break;
            default:
                break;
        }
    });

    if(gerenciados.length > 0 ) {
        avaliacoesMenu.style.display = 'block';
    }

    async function getGrupo(id) {
        const data = await genericApi(`grupos/${id}`, 'GET')
        return data;
    }
    async function getGerenciado(id) {
        const data = await genericApi(`colaboradores/${id}/gerenciados`, 'GET')
        return data;
    }
};