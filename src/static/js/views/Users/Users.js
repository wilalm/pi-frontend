import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";
export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Usuários");
    }

    async getHtml() {
        return `
            <h1>Usuários</h1>
            <nav class="nav__header">
            <div class="search__filter">
                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                <i class="fas fa-filter"></i>
            </div>
            <button class="button include new" style="margin: 15px 0 0 15px">Cadastrar</button>
            </nav>
            <div class="table__relatorios">
            <table id="relatorios" class="order-table table display">
                <thead>
                    <tr>
                        <th>Matrícula</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Data Cadastro</th>
                        <th>Grupo</th>
                        <th>Editar</th>
                        <th>Deletar</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        `;
    }

    init() {
        const LightTableFilter = (function (Arr) {

            let input = '';
            function onInputEvent(e) {
                input = e.target;
                let tables = document.getElementsByClassName(input.getAttribute('data-table'));
                Arr.forEach.call(tables, function (table) {
                    Arr.forEach.call(table.tBodies, function (tbody) {
                        Arr.forEach.call(tbody.rows, filter);
                    });
                });
            }

            function filter(row) {
                let text = row.textContent.toLowerCase(), val = input.value.toLowerCase();
                row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
            }

            return {
                init: function () {
                    const inputs = document.getElementsByClassName('light-table-filter');
                    Arr.forEach.call(inputs, function (input) {
                        input.oninput = onInputEvent;
                    });
                }
            };
        })(Array.prototype);
        LightTableFilter.init();

        const cadastrar = document.querySelector('.new');
        cadastrar.addEventListener(('click'), () => openModalNewUser());


        async function openModalNewUser() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal__include" id="modal__include">
                                <form class="register__form">
                                    <fieldset>
                                        <label for="nome">Colaborador</label>
                                        <input type="text" name="nome" id="colaboradorAvaliar" placeholder="Selecione um colaborador" value="" disabled>
                                    </fieldset>
                                    <fieldset>
                                        <label for="grupo">Grupo</label>
                                        <input type="text" name="grupo" id="grupo" placeholder="Selecione um Grupo" disabled>
                                    </fieldset>
                                    <fieldset>
                                        <label for="senha">Senha</label>
                                        <input type="password" name="senha" id="senhaNewUser" placeholder="Insira a senha">
                                    </fieldset>
                                    <fieldset>
                                        <label for="email">E-mail</label>
                                        <input type="email" name="email" id="email" placeholder="Insira o email">
                                    </fieldset>
                                </form>
                                    <div class="include__buttons">
                                        <button class="button include cadastrarUser">Cadastrar</button>
                                        <button class="button include associar__button newColaborador" style="margin: 0 0 5% 0">Selecionar Colaborador</button>
                                        <button class="button include associar__button newGrupo" style="margin: 0 0 5% 0">Selecionar Grupo</button>
                                        <button class="button cancel">Cancelar</button>
                                    </div>
                            <div>`;
            modal.innerHTML = content;

            const cadastrarGrupo = document.querySelector('.newGrupo');
            cadastrarGrupo.addEventListener(('click'), () => openIncludeModalGrupo());

            const cadastrarColaborador = document.querySelector('.newColaborador');
            cadastrarColaborador.addEventListener(('click'), () => openIncludeColaboradorModal());

            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.cadastrarUser').addEventListener('click', async () => {
                await include();
                searchUsers();
                modal.style.display = 'none';
            });
        }

        async function include() {
            const senha = document.getElementById('senhaNewUser').value;
            const email = document.getElementById('email').value;
            const params = {
                email,
                id_colaborador: Number(idColaborador),
                id_grupo: Number(idGrupo),
                senha,
            }

            await createUser(params);
            return;
        }

        async function editUser(id, userid) {
            const senha = document.getElementById('senhaNewUser').value;
            const email = document.getElementById('email').value;
            const params = {
                email,
                id_colaborador: Number(id),
                id_grupo: Number(idGrupo),
                senha,
            }

            await saveEdit(userid, params);
            return;
        }

        async function createUser(params) {
            const data = await genericApi(`usuarios`, 'POST', params)
            return data;
        }

        async function saveEdit(id, params) {
            const data = await genericApi(`usuarios/${id}`, 'PUT', params)
            return data;
        }

        async function openIncludeColaboradorModal() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Colaboradores</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p class="select">Selecione um Colaborador</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalCargos" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Matrícula</th>
                                                    <th>Nome</th>
                                                    <th>Cargo Atual</th>
                                                    <th style="display: none;">Id</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modal">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            await getColaboradores();
            colaboradoresListener();

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
                idColaborador = null;
            });
        }

        let idColaborador = null;
        async function colaboradoresListener() {
            const bodyTableModal = document.querySelector('.tbody__modal');
            const cargosModalSelect = bodyTableModal.getElementsByClassName('trCargo');
            for (let colaboradorTd of cargosModalSelect) {
                colaboradorTd.addEventListener('click', async () => {
                    idColaborador = colaboradorTd.querySelector('.td__modalId').innerText;
                    const nomeColaborador = colaboradorTd.querySelector('.td___modalName').innerText;
                    document.getElementById('colaboradorAvaliar').value = nomeColaborador;
                    closeSecondModal();
                })
            }
        }

        async function getColaboradores() {
            const response = await getAllColaboradores();
            let allColaboradores = response.content;
            let colaboradoresToCreate = [];
            allColaboradores.map((item) => {
                const find = users.find((filtro) => filtro.colaborador.id_colaborador == item.id_colaborador)
                if (!find) colaboradoresToCreate.push(item);
            });
            const tableModal = document.querySelector('.tbody__modal');
            if (colaboradoresToCreate.length > 0) {
                colaboradoresToCreate.map((colaborador) => {
                    const tr = `
                        <tr class="trCargo">
                            <td class="td__modalId">${colaborador.id_colaborador}</td>
                            <td class="td___modalName">${colaborador.nome}</td>
                            <td class="td__modalCargo">${colaborador.cargo.nome}</td>
                        </tr>`
                    tableModal.innerHTML += tr;
                })
            } else {
                document.querySelector('.select').innerHTML = '<b>Todos os colaboradores já possuem um usuário</b>'
            }
        }

        async function openIncludeModalGrupo() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Grupos</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Grupo</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalCargos" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Nome</th>
                                                    <th style="display: none">id</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modal">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            await getGrupos();
            gruposListener();

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
                idColaborador = null;
            });
        }

        let idGrupo = null;
        async function gruposListener() {
            const bodyTableModal = document.querySelector('.tbody__modal');
            const cargosModalSelect = bodyTableModal.getElementsByClassName('trCargo');
            for (let grupoTd of cargosModalSelect) {
                grupoTd.addEventListener('click', async () => {
                    idGrupo = grupoTd.querySelector('.td__modalId').innerText;
                    const nomeGrupo = grupoTd.querySelector('.td___modalName').innerText;
                    document.getElementById('grupo').value = nomeGrupo;
                    closeSecondModal();
                })
            }
        }

        async function getGrupos() {
            const response = await getAllGrupos();
            let allGrupos = response.content;
            const tableModal = document.querySelector('.tbody__modal');
            allGrupos.map((grupo) => {
                const tr = `
                    <tr class="trCargo">
                        <td class="td___modalName">${grupo.nome}</td>
                        <td class="td__modalId" style="display:none">${grupo.id_grupo}</td>
                    </tr>`
                tableModal.innerHTML += tr;
            })
        }


        function closeSecondModal() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'none';
        }

        async function getAllGrupos() {
            const data = await genericApi(`grupos`, 'GET')
            return data;
        }

        async function getAllColaboradores() {
            const data = await genericApi(`colaboradores`, 'GET')
            return data;
        }

        searchUsers();
        let users = null;
        async function searchUsers() {
            const response = await getUsers();
            users = response.content;
            if (users) {
                updateTable(users);
            } else {
                document.querySelector('.tittle__colaborador').innerHTML = '';
                document.querySelector('tbody').innerHTML = '';
                toastr.warningSearch('Nenhum usuário cadastrado');
            }
        }

        function buttonsListenerTable() {
            const tableRows = document.querySelectorAll('.colab');
            tableRows.forEach(row => {
                const id = row.querySelector('.idColab').innerText;
                const userid = row.querySelector('.idUser').innerText;
                const name = row.querySelector('.nameColab').innerText;
                row.querySelector('.deleteUser__button').addEventListener('click', () => openDeleteModal(id, name, userid));
                row.querySelector('.editUser__button').addEventListener('click', () => opendEditModal(id, name, userid))
            });
        }

        async function opendEditModal(id, nome, userid) {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal__include" id="modal__include">
                                <form class="register__form">
                                    <fieldset>
                                        <label for="nome">Colaborador</label>
                                        <input type="text" name="nome" id="colaboradorAvaliar" value="${nome}" disabled>
                                    </fieldset>
                                    <fieldset>
                                        <label for="grupo">Grupo</label>
                                        <input type="text" name="grupo" id="grupo" placeholder="Selecione um Grupo" disabled>
                                    </fieldset>
                                    <fieldset>
                                        <label for="senha">Senha</label>
                                        <input type="password" name="senha" id="senhaNewUser" placeholder="Insira a senha">
                                    </fieldset>
                                    <fieldset>
                                        <label for="email">E-mail</label>
                                        <input type="email" name="email" id="email" placeholder="Insira o email">
                                    </fieldset>
                                </form>
                                    <div class="include__buttons">
                                        <button class="button include cadastrarUser">Salvar</button>
                                        <button class="button include associar__button newGrupo" style="margin: 0 0 5% 0">Selecionar Grupo</button>
                                        <button class="button cancel">Cancelar</button>
                                    </div>
                            <div>`;
            modal.innerHTML = content;

            const cadastrarGrupo = document.querySelector('.newGrupo');
            cadastrarGrupo.addEventListener(('click'), () => openIncludeModalGrupo());

            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.cadastrarUser').addEventListener('click', async () => {
                await editUser(id, userid);
                searchUsers();
                modal.style.display = 'none';
            });
        }

        function openDeleteModal(id, tittle, userid) {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal">
                                        <i class="fas fa-times-circle delete__icon"></i>
                                        <p class="deleteTittle">Tem certeza?</p>
                                        <p>Realmente deseja deletar o usuário de <b>${tittle}</b>?</p>
                                        <b>Este processo não pode ser desfeito</b>
                                        <div class="delete__buttons">
                                            <button class="button cancel">Cancelar</button>
                                            <button class="button delete">Deletar</button>
                                        </div>
                                    <div>`;
            modal.innerHTML = content;
            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.delete').addEventListener('click', async () => {
                await deleteUser(userid)
                searchUsers();
                modal.style.display = 'none'
            });
        }

        async function deleteUser(id) {
            const data = await genericApi(`usuarios/${id}`, 'DELETE')
        }

        function updateTable(user) {
            if (user.length < 1) {
                const body = document.querySelector('tbody');
                body.innerHTML = '';
            } else {
                const body = document.querySelector('tbody');
                body.innerHTML = '';
                user.map((user) => {
                    let data = user.data_cadastro.replaceAll('-', '/');
                    data = data.replace('T', ' ');
                    const tr = `
                    <tr class="colab">
                        <td class="idUser" style="display: none">${user.id_usuario}</td>
                        <td data-label="Matrícula" class="idColab">${user.colaborador.id_colaborador}</td>
                        <td data-label="Nome" class="nameColab media_coment">${user.colaborador.nome}</td>
                        <td data-label="E-mail" class="media_coment">${user.email}</td>
                        <td data-label="Data Cadastro" class="media_coment">${data}</td>
                        <td data-label="Grupo">${user.grupo.nome}</td>
                        <td data-label="Editar" class="media_coment">
                            <button class="button editUser__button"><i class="fas fa-user-edit"></i></button>
                        </td>
                        <td data-label="Excluir" class="media_coment">
                            <button class="button deleteUser__button"><i class="fas fa-minus-circle"></i></button>
                        </td>
                    </tr>`
                    body.innerHTML += tr;
                })
                buttonsListenerTable();
            }
        }

        async function getUsers() {
            const data = await genericApi(`usuarios/?size=200`, 'GET')
            return data;
        };

        const toastr = {
            warningSearch(message) {
                const toastrModal = document.querySelector(".toastr");
                toastrModal.classList.add('show__toastr');
                const toastrmessage = toastrModal.querySelector(".toastr__message");
                toastrmessage.classList.add('toastr__warning');
                toastrmessage.innerHTML = `<p><b>Alerta</b></p>
                                            <p>${message}</p>`;
                setTimeout(function () {
                    toastrModal.classList.remove('show__toastr');
                    toastrmessage.classList.remove('toastr__warning');

                }, 3700);
            },
        };
    }
}
