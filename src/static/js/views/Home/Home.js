import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";
export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Home");
    }

    async getHtml() {
        return `
            <h1>Bem-vindo</h1>
            <div class="home__main">
                <div class="home__infos">
                    <div class="infos">
                        <h2>Minhas Informações</h2><br>
                        <p style="font-size: 18px" id="userName"></p><br>
                        <div class="container__infos">
                            <div class="dividerProfile">
                                <div class="container__input">
                                    <label class="input__info">
                                        <p>Cargo</p>
                                        <input id="userCargo" type="text" disabled/>
                                    </label>
                                </div>
                                <div class="container__input">
                                    <label class="input__info">
                                        <p>Departamento</p>
                                        <input id="userDepartamento" type="text" disabled/>
                                    </label>
                                </div>
                            </div>
                            <div class="dividerProfile">
                            <div class="container__input">
                            <label class="input__info">
                                        <p>Salário Atual</p>
                                        <input id="userSalario" type="text" disabled/>
                                    </label>
                                </div>
                                <div class="container__input">
                                    <label class="input__info">
                                        <p>Gestor</p>
                                        <input id="userGestor" type="text" disabled/>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="horario">
                        <h3>Horário</h3>
                        <div class="clock">
                            <div class="containerClock">
                                <span class="hours">00</span>
                            </div>
                            <div class="containerClock">
                                <span class="minutes">00</span>
                            </div>
                            <div class="containerClock">
                                <span class="seconds">00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="home__options">
                    <div class="container">
                        <div>
                            <p><i class="fas fa-file-alt"></i> Minhas Avaliações</p>
                        </div>
                        <div class="modal__content">
                            <table id="tableModalCargos" class="table" >
                                <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Gestor</th>
                                        <th>Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody class="tbody__modal" id="avaliacoes">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="container">
                        <div>
                            <p><i class="far fa-address-card"></i> Meus Benefícios</p>
                        </div>
                        <div class="modal__content">
                            <table id="tableModalCargos" class="table" >
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Valor</th>
                                        <th>Visualizar</th>
                                    </tr>
                                </thead>
                                <tbody class="tbody__modal" id="beneficios">
                                </tbody>
                            </table>
                        </div>
                    </div>  
                </div>
            </div>
            `;
    }

    init() {
        function clock() {
            var date = new Date(),
                hours = date.getHours(),
                minutes = date.getMinutes(),
                seconds = date.getSeconds();
            
            const hour = document.querySelectorAll(".hours")[0];
            if(hour) hour.innerHTML= zero(hours);

            const minute = document.querySelectorAll(".minutes")[0];
            if(minute) minute.innerHTML = zero(minutes);

            const second = document.querySelectorAll(".seconds")[0];
            if(second) second.innerHTML = zero(seconds);
            
            function zero(standIn) {
                if (standIn < 10) {
                    standIn = "0" + standIn
                }
                return standIn;
            }
        }
        setInterval(clock, 1000);

        const user = JSON.parse(localStorage.getItem("user"));

        document.getElementById('userName').innerHTML = `Você está logado como: ${user.nome}`;
        document.getElementById('userCargo').value = `${user.cargo.nome}`;
        document.getElementById('userDepartamento').value = `${user.departamento.nome}`;
        document.getElementById('userGestor').value = `${user.gestor?.nome || ''}`;
        document.getElementById('userSalario').value = user.salario.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });

        setAvaliacoes(user);
        setBeneficios(user);

        function setAvaliacoes(user) {
            const avaliacoes = document.getElementById('avaliacoes');
            avaliacoes.innerHTML = '';
            if(user.avaliacoes.length > 0) {                
                user.avaliacoes.map((avaliacao) => {
                    const trAvaliacao = document.createElement('tr');
                    
                    const tdData = document.createElement('td');
                    tdData.innerHTML = `${avaliacao.data.replaceAll('-', '/')}`;
                    tdData.setAttribute('data-label', 'Data');
                    trAvaliacao.appendChild(tdData);

                    const tdGestor = document.createElement('td');
                    tdGestor.innerHTML = `${avaliacao.nome_gestor}`;
                    tdGestor.setAttribute('data-label', 'Gestor');
                    tdGestor.classList.add('media_coment')
                    trAvaliacao.appendChild(tdGestor);

                    const tdView = document.createElement('td');
                    tdView.innerHTML = `<i class="fas fa-info-circle"></i>`;
                    tdView.setAttribute('data-label', 'Visualizar');
                    tdView.addEventListener('click', () => {
                        openViewModalAvaliacao(avaliacao);
                    });
                    trAvaliacao.appendChild(tdView);

                    avaliacoes.appendChild(trAvaliacao);
                })
            }
        }

        async function setBeneficios(user) {
            const beneficios = await getBenefits(user.cargo.id_cargo);
            const tableBeneficios = document.getElementById('beneficios');
            tableBeneficios.innerHTML = '';
            if(beneficios.length > 0) {
                beneficios.map((beneficios) => {
                    const trBeneficio = document.createElement('tr');
                    
                    const tdNome = document.createElement('td');
                    tdNome.innerHTML = `${beneficios.nome}`;
                    tdNome.classList.add('media_coment')
                    tdNome.setAttribute('data-label', 'Nome');
                    trBeneficio.appendChild(tdNome);

                    const tdValor = document.createElement('td');
                    tdValor.innerHTML = `${beneficios.valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}`;
                    tdValor.setAttribute('data-label', 'Valor');
                    trBeneficio.appendChild(tdValor);

                    const tdView = document.createElement('td');
                    tdView.innerHTML = `<i class="fas fa-info-circle"></i>`;
                    tdView.setAttribute('data-label', 'Visualizar');
                    tdView.addEventListener('click', () => {
                        openViewModalBeneficio(beneficios);
                    });
                    trBeneficio.appendChild(tdView);

                    tableBeneficios.appendChild(trBeneficio);
                })
            }
        }

        function openViewModalBeneficio(beneficio) {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                <div class="modalHeader">
                                    <h4 style="font-size:20px">Benefício</h4>
                                </div>
                                <div class="modal__content">
                                    <form class="form__avaliacao">
                                        <div>
                                            <label class="input-field">
                                                <p>Nome</p>
                                                <input value="${beneficio.nome}" type="text" disabled/>
                                            </label>
                                            <label class="input-field">
                                                <p>Valor</p>
                                                <input value="${beneficio.valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}" type="text" disabled/>
                                            </label>
                                        </div>
                                        <div>
                                            <label class="input-field">
                                                <p>Descrição</p>
                                                <textarea class="avaliacao__comentario" rows="5" cols="66">${beneficio.descricao}</textarea>
                                            </label>
                                        </div>
                                    </form>
                                </div>
                                <button style="margin-bottom: 3%" class="button cancel">Fechar</button>
                            <div>`;
            modal.innerHTML = content;

            const cancel = document.querySelector('.cancel');
            cancel.addEventListener('click', () => closeModal());
        }

        function openViewModalAvaliacao(avaliacao) {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                <div class="modalHeader">
                                    <h4 style="font-size:20px">Avaliação</h4>
                                </div>
                                <div class="modal__content">
                                    <form class="form__avaliacao">
                                        <div>
                                            <label class="input-field">
                                                <p>Data da Avaliação</p>
                                                <input value="${avaliacao.data.replaceAll('-', '/')}" type="text" disabled/>
                                            </label>
                                            <label class="input-field">
                                                <p>Gestor</p>
                                                <input value="${avaliacao.nome_gestor}" type="text" disabled/>
                                            </label>
                                        </div>
                                        <div>
                                            <label class="input-field">
                                                <p>Nota Comportamento</p>
                                                <input value="${avaliacao.nota_comportamento}" type="text" disabled/>
                                            </label>
                                            <label class="input-field">
                                                <p>Nota Técnica</p>
                                                <input value="${avaliacao.nota_tecnica}" type="text" disabled/>
                                            </label>
                                            <label class="input-field">
                                                <p>Nota Trabalho em Equipe</p>
                                                <input value="${avaliacao.nota_trabalho_em_equipe}" type="text" disabled/>
                                            </label>
                                        </div>
                                        <div>
                                            <label class="input-field">
                                                <p>Comentário</p>
                                                <textarea class="avaliacao__comentario" rows="5" cols="66" id="comentario">${avaliacao.comentario}</textarea>
                                            </label>
                                        </div>
                                    </form>
                                </div>
                                <button style="margin-bottom: 3%" class="button cancel">Fechar</button>
                            <div>`;
            modal.innerHTML = content;

            const cancel = document.querySelector('.cancel');
            cancel.addEventListener('click', () => closeModal());
        }

        function closeModal(params) {
            const modal = document.querySelector('.fade');
            modal.style.display = 'none';
        }

        async function getBenefits(id) {
            const data = await genericApi(`cargos/${id}/beneficios/?=200`, 'GET')
            return data;
        }
    }
}