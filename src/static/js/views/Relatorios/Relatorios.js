import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";
export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Relatórios");
    }

    async getHtml() {
        return `
            <h1>Relatórios</h1>
            <nav class="nav__header">
            <div class="search__filter">
                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                <i class="fas fa-filter"></i>
            </div>
            </nav>
            <div class="table__relatorios">
            <table id="relatorios" class="order-table table display">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Paramêtro (Obrigatório)</th>
                        <th>Gerar Relatório</i></th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="custoBeneficio" style="display:none">
                        <td data-label="Nome">Custo Total por Benefício</td>
                        <td class="media_coment" >Nenhum Paramêtro Obrigatório</td>
                        <td data-label="Download" class="download_custo_total_beneficio" style="cursor: pointer"><i class="fas fa-download"></i></td>
                    </tr>
                    <tr id="diversidadeIdade" style="display:none">
                        <td data-label="Nome">Diversidade Idade</td>
                        <td class="media_coment" ><button class="button include date_diversidade">Selecionar Data</button></td>
                        <td data-label="Download" class="diversidade_idade" style="cursor: pointer"><i class="fas fa-download"></i></td>
                    </tr>
                    <tr id="eventosColaborador" style="display:none">
                        <td data-label="Nome">Eventos Colaborador</td>
                        <td class="media_coment" >
                            <button class="button include colaboradores_eventos" style="display:none"> Colaborador</button>
                            <button class="button include date_eventos">Selecionar Data</button>
                        </td>
                        <td data-label="Download" class="donwload_eventos_colaborador" style="cursor: pointer"><i class="fas fa-download"></i></td>
                    </tr>
                </tbody>
            </table>
            </div>
        `;
    }

    init() {
        const colaborador = JSON.parse(localStorage.getItem("user"));
        const user = JSON.parse(localStorage.getItem("user"));

        if(user.departamento.nome == 'RH') {
            const button = document.querySelector('.colaboradores_eventos');
            button.style.display = 'block';
        }

        async function getGrupo(id) {
            const data = await genericApi(`grupos/${id}`, 'GET')
            return data;
        }

        async function checkPermission() {
            const grupo = await getGrupo(user.usuario[0].grupo.id_grupo)
            const permissoes = grupo.permissoes

            const eventosColaborador = permissoes.find(filtro => filtro.id_permissao == 10)
            if(eventosColaborador) {
                document.querySelector('#eventosColaborador').style.display = 'table-row';
            }

            const diversidadeIdade = permissoes.find(filtro => filtro.id_permissao == 11)
            if(diversidadeIdade) {
                document.querySelector('#diversidadeIdade').style.display = 'table-row';
            }

            const custoBeneficio = permissoes.find(filtro => filtro.id_permissao == 12)
            if(custoBeneficio) {
                document.querySelector('#custoBeneficio').style.display = 'table-row';
            }
        }
        checkPermission();

        const LightTableFilter = (function(Arr) {

            let input = '';
            function onInputEvent(e) {
                input = e.target;
                let tables = document.getElementsByClassName(input.getAttribute('data-table'));
                Arr.forEach.call(tables, function(table) {
                    Arr.forEach.call(table.tBodies, function(tbody) {
                        Arr.forEach.call(tbody.rows, filter);
                    });
                });
            }
    
            function filter(row) {
                let text = row.textContent.toLowerCase(), val = input.value.toLowerCase();
                row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
            }
    
            return {
                init: function() {
                    const inputs = document.getElementsByClassName('light-table-filter');
                    Arr.forEach.call(inputs, function(input) {
                        input.oninput = onInputEvent;
                    });
                }
            };
        })(Array.prototype);
        LightTableFilter.init();


        const toastr = {
            warningSearch(message) {
                const toastrModal = document.querySelector(".toastr");
                toastrModal.classList.add('show__toastr');
                const toastrmessage = toastrModal.querySelector(".toastr__message");
                toastrmessage.classList.add('toastr__warning');
                toastrmessage.innerHTML = `<p><b>Alerta</b></p>
                                            <p>${message}</p>`;
                setTimeout(function(){ 
                    toastrModal.classList.remove('show__toastr');                
                    toastrmessage.classList.remove('toastr__warning');
 
                }, 3700);
            },
        };

        const custoTotalPorBeneficio = document.querySelector('.download_custo_total_beneficio');
        custoTotalPorBeneficio.addEventListener('click', () => {
            fetch('http://localhost:8080/relatorios/custo-total-atual-por-beneficio', {
                method: 'POST',
                cache: 'default',
                mode: 'cors',
                body: JSON.stringify({}),
                headers: { 'Content-type': 'application/json' },
            })
                .then(response => response.blob())
                    .then(showFileCustoTotalBeneficio)
        })

        function showFileCustoTotalBeneficio(blob){
            const newBlob = new Blob([blob], {type: "application/pdf"})
    
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
            } 
          
            const data = window.URL.createObjectURL(newBlob);
            const link = document.createElement('a');
            link.href = data;
            link.download = "custo-total-por-beneficio.pdf";
            link.click();
            setTimeout(function(){
              window.URL.revokeObjectURL(data);
            }, 100);
        } 

        let data_fim_diversidade = "17/06/2021";
        let data_inicio_diversidade = "01/01/2021";
        const diversidadeIdade = document.querySelector('.diversidade_idade');
        diversidadeIdade.addEventListener('click', () => {
            fetch('http://localhost:8080/relatorios/diversidade-idade', {
                method: 'POST',
                cache: 'default',
                mode: 'cors',
                body: JSON.stringify({  
                    "data_fim": data_fim_diversidade,
                    "data_inicio": data_inicio_diversidade
                }),
                headers: { 'Content-type': 'application/json' },
            })
                .then(response => response.blob())
                    .then(showFileDiversidadeIdade)
        })


        function showFileDiversidadeIdade(blob){
            const newBlob = new Blob([blob], {type: "application/pdf"})
    
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
            } 
          
            const data = window.URL.createObjectURL(newBlob);
            const link = document.createElement('a');
            link.href = data;
            link.download = "diversidade-idade.pdf";
            link.click();
            setTimeout(function(){
              window.URL.revokeObjectURL(data);
            }, 100);
        }
        
        let data_fim_eventos = "17/06/2021";
        let data_inicio_eventos = "01/01/2021";
        let id_colaborador = colaborador.id_colaborador;
        const eventosColaborador = document.querySelector('.donwload_eventos_colaborador');
        eventosColaborador.addEventListener('click', () => {
            fetch('http://localhost:8080/relatorios/eventos-colaborador', {
                method: 'POST',
                cache: 'default',
                mode: 'cors',
                body: JSON.stringify({  
                    "data_fim": data_fim_eventos,
                    "data_inicio": data_inicio_eventos,
                    "id_colaborador": Number(id_colaborador) || Number(colaborador.id_colaborador)
                }),
                headers: { 'Content-type': 'application/json' },
            })
                .then(response => response.blob())
                    .then(showFileEventos)
        })


        function showFileEventos(blob){
            const newBlob = new Blob([blob], {type: "application/pdf"})
    
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
            } 
          
            const data = window.URL.createObjectURL(newBlob);
            const link = document.createElement('a');
            link.href = data;
            link.download = "eventos-colaborador.pdf";
            link.click();
            setTimeout(function(){
              window.URL.revokeObjectURL(data);
            }, 100);
        }
        
        document.querySelector('.date_diversidade').addEventListener('click', () => openDateDiversidade())
        async function openDateDiversidade() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal__include">
                                    <b>Selecione o Período</b>
                                    <form class="register__form">
                                        <fieldset>
                                            <p>Data Inicio</p>
                                            <input class="data_inicio_select"  type="date"/>
                                        </fieldset>
                                        <fieldset>
                                            <p>Data Fim</p>
                                            <input class="data_fim_select" type="date"/>
                                        </fieldset>
                                    </form>
                                    <div class="include__buttons" style="margin: 0 0 25px 0">
                                        <button class="button cancel cancelDiversidade">Cancelar</button>
                                        <button class="button include confirmDiversidade">Confirmar</button>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            document.querySelector('.cancelDiversidade').addEventListener('click', () => {
                modal.style.display = 'none';
            });
            document.querySelector('.confirmDiversidade').addEventListener('click', () => {
                let dataFim = document.querySelector('.data_fim_select').value;
                dataFim = tranformDate(dataFim);

                let dataInicio = document.querySelector('.data_inicio_select').value;
                dataInicio = tranformDate(dataInicio);

                data_fim_diversidade = dataFim || "17/06/2021";
                data_inicio_diversidade = dataInicio || "01/01/2021";
                modal.style.display = 'none';
            });
        }
        
        document.querySelector('.date_eventos').addEventListener('click', () => openDateEventos())
        async function openDateEventos() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal__include">
                                    <b>Selecione o Período</b>
                                    <form class="register__form">
                                        <fieldset>
                                            <p>Data Inicio</p>
                                            <input class="data_inicio_select_eventos"  type="date"/>
                                        </fieldset>
                                        <fieldset>
                                            <p>Data Fim</p>
                                            <input class="data_fim_select_eventos" type="date"/>
                                        </fieldset>
                                    </form>
                                    <div class="include__buttons" style="margin: 0 0 25px 0">
                                        <button class="button cancel cancelEventos">Cancelar</button>
                                        <button class="button include confirmEventos">Confirmar</button>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            document.querySelector('.cancelEventos').addEventListener('click', () => {
                modal.style.display = 'none';
            });
            document.querySelector('.confirmEventos').addEventListener('click', () => {
                let dataFim = document.querySelector('.data_fim_select_eventos').value;
                dataFim = tranformDate(dataFim);

                let dataInicio = document.querySelector('.data_inicio_select_eventos').value;
                dataInicio = tranformDate(dataInicio);

                data_fim_eventos = dataFim || "17/06/2021";
                data_inicio_eventos = dataInicio || "01/01/2021";
                modal.style.display = 'none';
            });
        }

        document.querySelector('.colaboradores_eventos').addEventListener('click', () => setColaborador())
        async function setColaborador(card) {
            const modal = document.querySelector('.fade');
            modal.innerHTML = '';
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Colaboradores</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Colaborador</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalGestorRecomendados" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th style="display:none">Matricula</th>
                                                    <th>Colaborador</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modalGestor">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            await setGestoresEdit(card, modal);

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
            });
        }

        async function setGestoresEdit(card, modal) {
            const response = await get();
            let all = response.content;
            const tableModal = document.querySelector('.tbody__modalGestor');
            all.map((gestor) => {
                const tr = `
                    <tr class="trGestor">
                        <td class="td___modalName">${gestor.nome}</td>
                        <td class="td__modalId" style="display:none">${gestor.id_colaborador}</td>
                    </tr>`
                tableModal.innerHTML += tr;
            })

            const gestorModalSelect = tableModal.getElementsByClassName('trGestor');
            for (let gestor of gestorModalSelect) {
                gestor.addEventListener('click', async () => {
                    id_colaborador = gestor.querySelector('.td__modalId').innerText;
                    modal.style.display = 'none';
                })
            }
        }

        async function get() {
            const data = await genericApi(`colaboradores/?size=200`, 'GET')
            return data;
        }

        function tranformDate(strDate) {
            let result = '';
        
            if (strDate) {
              let parts = strDate.split('-');
              result = `${parts[2]}/${parts[1]}/${parts[0]}`;
            }
            return result;
        }
    }
}
