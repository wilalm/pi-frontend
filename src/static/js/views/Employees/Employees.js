import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Colaboradores");
    }

    async getHtml() {
        return `
            <h1>Colaboradores</h1>
            <nav class="nav__header">
                <div class="search__wrapper">
                    <div class="search__box">
                        <div class="search__dropdown">
                            <div class="search__default__option">Nome</div>  
                            <ul>
                                <li value="Todos">Nome</li>
                                <li value="Recent">CPF</li>
                                <li value="Popular">Matrícula</li>
                            </ul>
                        </div>
                    <div class="search__field">
                        <input type="text" class="search__input" placeholder="Pesquisar">
                        <i class="fas fa-search"></i>
                    </div>
                    </div>
                </div>
                <div class="paginate__wrapper">
                    <button class="button insert__button new">Cadastrar</button>
                    <div class="controls">
                        <div class="first">&#171;</div>
                        <div class="prev"><</div>
                        <div class="numbers">
                            <div>1</div>
                        </div>
                        <div class="next">></div>
                        <div class="last">&#187;</div>
                    </div>
                </div>
            </nav>
            <div id="list" class="list"><div>
        `;
    }

    // ***
    // INICIALIZA O SCRIPT APOS O HTML CARREGAR
    // ***
    init() {
        listItems();

        async function get() {
            const data = await genericApi(`colaboradores/?size=200`, 'GET')
            return data;
        }

        async function getCargos() {
            const data = await genericApi(`cargos/?size=200`, 'GET')
            return data;
        }

        async function getDepartamentos() {
            const data = await genericApi(`departamentos/?size=200`, 'GET')
            return data;
        }

        async function deleteItem(id) {
            const data = await genericApi(`colaboradores/${id}`, 'DELETE')
        }

        async function create(params) {
            const data = await genericApi(`colaboradores`, 'POST', params)
        }

        async function edit(id, params) {
            const data = await genericApi(`colaboradores/${id}`, 'PUT', params)
        }

        async function listItems() {
            const data = await get();
            items = data.content;
            let showItems = checkItems(items);
            state.totalPage = Math.ceil(showItems.length / state.perPage);
            if (showItems.length === 0) {
                document.getElementById('list').innerHTML = "";
                let card = document.createElement("div");
                card.classList.add('cardList');
                const text = `<p>Nenhum Colaborador Encontrado</p>`;
                card.innerHTML = text;
                document.querySelector('#list').append(card);
            } else {
                list.update();
                controllerButtons.update();
            }
        }
        
        function checkItems(itens) {
            const type = searchDropdown.innerText;
            const value = document.querySelector('.search__input').value;
            let showCards = [];
            switch(type) {
                case 'Nome': {
                    if(value) {
                        itens.map((filtro) => {
                            if(filtro.nome.indexOf(value) !== -1) {
                                showCards.push(filtro);
                            }
                        })
                    }
                };
                case 'CPF': {
                    if(value) {
                        itens.map((filtro) => {
                            if(value === filtro.cpf) {
                                showCards.push(filtro);
                            }
                        })
                    }
                };
                case 'Matrícula': {
                    if(value) {
                        itens.map((filtro) => {
                            if(value == filtro.id_colaborador) {
                                showCards.push(filtro);
                            }
                        })
                    }
                };
                default:
                    break;
            }
            if(value == '' || value == null) {
                showCards = items;
            }
            return showCards;
        }
        /*
        * Cria um card
        * @item - Dados do benefício para popular o card
        */
        function createCard(item) {
            let card = document.createElement("div");
            card.classList.add('cardList');
            card.id = item.id_colaborador;
            const text = `<p class="card__tittle">${item.nome}</p>
                            <form class="inputs__wrapper">
                                <div>
                                    <label class="input-field">
                                        <p>RG</p>
                                        <input name="rg" value="${item.rg.replace(/(\d{2})(\d{3})(\d{3})(\d{1})$/,"$1.$2.$3-$4")}" type="text" disabled/>
                                    </label>
                                    <label class="input-field">
                                        <p>Data Nascimento</p>
                                        <input name="data_nascimento" value="${item.data_nascimento}" type="date" disabled/>
                                    </label>
                                </div>
                                <div>
                                    <label class="input-field">
                                        <p>CPF</p>
                                        <input name="cpf" value="${item.cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{1,2})/g, "$1.$2.$3-$4")}" type="text" disabled/>
                                    </label>
                                    <label class="input-field">
                                        <p>Data Contratação</p>
                                        <input name="data_contratacao" value="${item.data_contratacao}" type="date" disabled/>
                                    </label>
                                </div>
                                <div>
                                    <label class="input-field">
                                        <p>CTPS</p>
                                        <input name="ctps" value="${item.ctps}" type="text" disabled/>
                                    </label>
                                    <label class="input-field">
                                        <p>Salário</p>
                                        <input name="salario" value="${item.salario.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}" type="text" disabled/>
                                    </label>
                                </div>
                                <div>
                                    <label class="input-field">
                                        <p class="tittleDepto">Departamento</p>
                                        <p class="idDepartamento" style="display: none">${item.departamento.id_departamento}</p>
                                        <input class="departamento" name="departamento" value="${item.departamento.nome}" type="text" disabled/>
                                    </label>
                                    <label class="input-field">
                                        <p>Cargo</p>
                                        <input name="cargo" value="${item.cargo.nome}" type="text" disabled/>
                                    </label>
                                </div>
                                <div>
                                    <label class="input-field">
                                        <p class="tittleGestor">Gestor</p>
                                        <p class="idGestor" style="display: none">${item.gestor?.id_colaborador}</p>
                                        <input name="gestor" class="gestor" value="${item.gestor?.nome || ''}" type="text" disabled/>
                                    </label>
                                    <label class="input-field">
                                    <p>Matrícula</p>
                                    <input name="matricula" value="${item.id_colaborador}" type="text" disabled/>
                                </label>
                                </div>
                            </form>`
            const buttons = `<div class="card__buttons">
                                <button class="button events__button"><i class="fas fa-history"></i><a href="/colaborador/${card.id}" data-link>Histórico</a></button>
                                <button class="button edit__button"><i class="fas fa-edit"></i>Editar</button>
                                <button class="button delete__button"><i class="far fa-trash-alt"></i>Deletar</button>
                            </div>`
            card.innerHTML = text + buttons;
            document.getElementById('list').appendChild(card);
        }

        function setColaborador(id, tittle) {
            const colaborador = {
                id: id,
                nome: tittle,
            };
            localStorage.setItem("colaboradorHistorico", JSON.stringify(colaborador))
        }

        /*
        * Observa a ação de click dos botões do card
        */
        function cardButtonsListeners() {
            const cards = document.querySelectorAll('.cardList');
            cards.forEach((card) => {
                const id = card.id;
                const tittle = card.querySelector('.card__tittle').innerText;

                const deleteButton = card.querySelector('.delete__button');
                const editButton = card.querySelector('.edit__button');
                const historico = card.querySelector('.events__button');
                
                historico.addEventListener('click', () => setColaborador(id, tittle));
                deleteButton.addEventListener('click', () => openDeleteModal(id, tittle));
                editButton.addEventListener('click', () => editCard(card));
            })
        };

        function editCard(card) {
            const form = card.querySelector('form');
            form.querySelector('input[name="rg"]').disabled = false;
            form.querySelector('input[name="cpf"]').disabled = false;
            form.querySelector('input[name="ctps"]').disabled = false;
            form.querySelector('input[name="data_nascimento"]').disabled = false;

            form.querySelector('.tittleDepto').innerHTML = 'Departamento <i class="fas fa-external-link-alt" style="font-size: 18px; cursor: pointer;"></i>'
            form.querySelector('.tittleGestor').innerHTML = 'Gestor <i class="fas fa-external-link-alt" style="font-size: 18px; cursor: pointer;"></i>'

            const cardButtons = card.querySelector('.card__buttons');
            cardButtons.style.justifyContent = 'space-between';
            const buttons = `
                            <button class="button cancel">Cancelar</button>
                            <button class="button save">Salvar</button>
                            `
            cardButtons.innerHTML = buttons;
            
            card.querySelector('.tittleDepto').addEventListener('click', () => setDepartamentoEdit(card))
            card.querySelector('.tittleGestor').addEventListener('click', () => setGestorEdit(card))
            card.querySelector('.cancel').addEventListener('click', () => {
                listItems();
                closeEdit(card, form)
            }); 
            card.querySelector('.save').addEventListener('click', () => saveCard(card, form));
        };

        function setDepartamentoEdit(card) {
            const modal = document.querySelector('.secondFade');
            modal.innerHTML = '';
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Departamentos</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Departamento</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalCargosRecomendados" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Departamento</th>
                                                    <th style="display: none">id</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modalDepartamento">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            setDepartamentosEdit(card, modal);

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
            });
        }

        async function setGestorEdit(card) {
            const modal = document.querySelector('.fade');
            modal.innerHTML = '';
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Gestor</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Colaborador</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalGestorRecomendados" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th style="display:none">Matricula</th>
                                                    <th>Colaborador</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modalGestor">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            await setGestoresEdit(card, modal);

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
            });
        }

        let idDepartamentoEdit = null;
        async function setDepartamentosEdit(card, modal) {
            const response = await getDepartamentos();
            let allDepartamentos = response.content;
            const tableModal = document.querySelector('.tbody__modalDepartamento');
            allDepartamentos.map((departamento) => {
                const tr = `
                    <tr class="trDepartamento">
                        <td class="td___modalName">${departamento.nome}</td>
                        <td class="td__modalId" style="display:none">${departamento.id_departamento}</td>
                    </tr>`
                tableModal.innerHTML += tr;
            })

            const departamentoModalSelect = tableModal.getElementsByClassName('trDepartamento');
            for (let departamentoTd of departamentoModalSelect) {
                departamentoTd.addEventListener('click', async () => {
                    idDepartamentoEdit = departamentoTd.querySelector('.td__modalId').innerText;
                    const nomeDepartamento = departamentoTd.querySelector('.td___modalName').innerText;
                    card.querySelector('.departamento').value = nomeDepartamento;
                    modal.style.display = 'none';
                })
            }
        }

        let idGestorEdit = null;
        async function setGestoresEdit(card, modal) {
            const response = await get();
            let all = response.content;
            const tableModal = document.querySelector('.tbody__modalGestor');
            all.map((gestor) => {
                const tr = `
                    <tr class="trGestor">
                        <td class="td___modalName">${gestor.nome}</td>
                        <td class="td__modalId" style="display:none">${gestor.id_colaborador}</td>
                    </tr>`
                tableModal.innerHTML += tr;
            })

            const gestorModalSelect = tableModal.getElementsByClassName('trGestor');
            for (let gestor of gestorModalSelect) {
                gestor.addEventListener('click', async () => {
                    idGestorEdit = gestor.querySelector('.td__modalId').innerText;
                    const nomeGestor = gestor.querySelector('.td___modalName').innerText;
                    card.querySelector('.gestor').value = nomeGestor;
                    modal.style.display = 'none';
                })
            }
        }

        async function saveCard(card, form) {
            const id = card.id;
            let nome = card.querySelector('.card__tittle').innerText;
            let rg = card.querySelector('input[name="rg"]').value
            rg = rg.replaceAll('.', '');
            rg = rg .replace('-','');
            let cpf = card.querySelector('input[name="cpf"]').value
            cpf = cpf.replaceAll('.', '');
            cpf = cpf.replace('-','');
            let ctps = card.querySelector('input[name="ctps"]').value
            let data_contratacao = card.querySelector('input[name="data_contratacao"]').value
            let data_nascimento = card.querySelector('input[name="data_nascimento"]').value

            const params = {
                nome,
                rg,
                cpf,
                ctps,
                data_contratacao,
                data_nascimento,
                id_departamento: Number(idDepartamentoEdit) || Number(card.querySelector('.idDepartamento').innerText),
                id_gestor: Number(idGestorEdit) || Number(card.querySelector('.idGestor').innerText)
            }

            await edit(id, params);
            listItems();
            closeEdit(card);
        }

        function closeEdit(card, form) {
            const inputs = card.querySelectorAll('input');
            inputs.forEach((input) => {
                input.disabled = true;
            })

            const cardButtons = card.querySelector('.card__buttons');
            cardButtons.style.justifyContent = 'flex-end';
            const buttons = `
                            <button class="button events__button"><i class="fas fa-history"></i><a href="/colaborador/${card.id}" data-link>Histórico</a></button>
                            <button class="button edit__button"><i class="fas fa-edit"></i>Editar</button>
                            <button class="button delete__button"><i class="far fa-trash-alt"></i>Deletar</button>
                            `
            cardButtons.innerHTML = buttons;
            cardButtonsListeners();
            if(form) {
                card.querySelector('form').innerHTML = form.innerHTML;
            }
        }

        /*
        * Cria modal de confirmação antes de deletar
        */
        function openDeleteModal(id, tittle) {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal">
                                <i class="fas fa-times-circle delete__icon"></i>
                                <p class="deleteTittle">Tem certeza?</p>
                                <p>Realmente deseja deletar <b>${tittle}</b>?</p>
                                <b>Este processo não pode ser desfeito</b>
                                <div class="delete__buttons">
                                    <button class="button cancel">Cancelar</button>
                                    <button class="button delete">Deletar</button>
                                </div>
                            <div>`;
            modal.innerHTML = content;
            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.delete').addEventListener('click', async () => {
                await deleteItem(id)
                listItems();
                modal.style.display = 'none'
            });
        }

        /*
        * Search Input
        */
        const searchDropdown = document.querySelector(".search__default__option")
        const searchDropdowmList = document.querySelector(".search__dropdown ul")
        const searchDropdownItem = document.querySelectorAll(".search__dropdown ul li");
        searchDropdown.addEventListener('click', () => searchDropdowmList.classList.toggle('active'));
        searchDropdownItem.forEach((item) => {
            item.addEventListener('click', function (event) {
                const text = this.innerHTML;
                searchDropdown.innerHTML = text;
                searchDropdowmList.classList.toggle('active');                
            })
        })

        const pesquisar = document.querySelector('.fa-search');
        pesquisar.addEventListener(('click'), () => listItems());

        const cadastrar = document.querySelector('.new');
        cadastrar.addEventListener(('click'), () => openIncludeModal());

        /*
        * Cria modal de confirmação antes de deletar
        */
        async function openIncludeModal() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal__include">
                                <b>Cadastro de Colaborador</b>
                                <form class="register__form">
                                    <fieldset>
                                        <label for="nome">Nome do Colaborador</label>
                                        <input type="text" name="nome" id="nome" placeholder="Insira o nome" required>
                                    </fieldset>
                                    <fieldset>
                                        <label for="rg">RG</label>
                                        <input type="text" name="rg" id="rg" placeholder="Insira o RG" required>
                                    </fieldset>
                                    <fieldset>
                                        <label for="cpf">CPF</label>
                                        <input type="text" name="cpf" id="cpf" placeholder="Insira o CPF" required>
                                    </fieldset>
                                    <fieldset>
                                        <label for="ctps">CTPS</label>
                                        <input type="text" name="ctps" id="ctps" placeholder="Insira o CTPS" required>
                                    </fieldset>
                                    <fieldset>
                                        <label for="data_nascimento">Data de Nascimento</label>
                                        <input type="date" name="data_nascimento" id="data_nascimento" required>
                                    </fieldset>
                                    <fieldset>
                                        <label for="data_contratacao">Data de Contratação</label>
                                        <input type="date" name="data_contratacao" id="data_contratacao" required>
                                    </fieldset>
                                    <fieldset>
                                        <label for="salario">Salário</label>
                                        <input type="text" name="salario" id="salario" placeholder="Informe o salário inicial" required>
                                    </fieldset>
                                    <fieldset>
                                        <label for="departamento" id="selectDepartamento">Departamento <i class="fas fa-external-link-alt" style="font-size: 18px; cursor: pointer"></i></i></label>
                                        <input type="text" name="departamento" id="departamentoSelected" placeholder="Selecione o departamento" disabled required>
                                    </fieldset>
                                    <fieldset>
                                        <label for="gestor" id="selectGestor">Gestor <i class="fas fa-external-link-alt" style="font-size: 18px; cursor: pointer"></i></label>
                                        <input type="text" name="gestor" id="gestorSelected" placeholder="Selecione o gestor" disabled required>
                                    </fieldset>
                                    <fieldset>
                                        <label for="cargo" id="selectCargo">Cargo <i class="fas fa-external-link-alt" style="font-size: 18px; cursor: pointer"></i></label>
                                        <input type="text" name="cargo" id="cargoSelected" placeholder="Selecione o cargo" disabled required>
                                    </fieldset>
                                    <div class="include__buttons">
                                        <button class="button include" type="submit" style="margin: 10px">Cadastrar</button>
                                        <button class="button cancel" style="margin: 10px">Cancelar</button>
                                    </div>
                                </form>
                            <div>`;
            modal.innerHTML = content;
            modal.querySelector('#selectDepartamento').addEventListener('click', () => openModalDepartamentoRegister())
            modal.querySelector('#selectGestor').addEventListener('click', () => openModalGestorRegister())
            modal.querySelector('#selectCargo').addEventListener('click', () => openModalCargoRegister())
            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.register__form').addEventListener('submit', async (event) => {
                event.preventDefault();
                include().then(() => {
                    listItems();
                })
                modal.style.display = 'none';
            });
        }

        async function openModalDepartamentoRegister() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Departamentos</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Departamento</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalCargosRecomendados" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Departamento</th>
                                                    <th style="display: none">id</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modalDepartamento">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            setDepartamentosRegister();

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
            });
        }

        async function openModalCargoRegister() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Cargos</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Cargo</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalCargosRecomendados" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Cargos</th>
                                                    <th style="display: none">id</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modalCargosRegister">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            await setCargosRegister();

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
            });
        }

        async function openModalGestorRegister() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Gestor</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Colaborador</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalGestorRecomendados" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Matricula</th>
                                                    <th>Colaborador</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modalGestorRegister">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            await setGestorRegister();

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
            });
        }

        let idCargoRegister = null;
        async function setCargosRegister() {
            const response = await getCargos();
            let allCargos = response.content;
            const tableModal = document.querySelector('.tbody__modalCargosRegister');
            allCargos.map((cargo) => {
                const tr = `
                    <tr class="trCargo">
                        <td class="td___modalName">${cargo.nome}</td>
                        <td class="td__modalId" style="display:none">${cargo.id_cargo}</td>
                    </tr>`
                tableModal.innerHTML += tr;
            })

            const cargosModalSelect = tableModal.getElementsByClassName('trCargo');
            for (let cargoTd of cargosModalSelect) {
                cargoTd.addEventListener('click', async () => {
                    idCargoRegister = cargoTd.querySelector('.td__modalId').innerText;
                    const nomeCargo = cargoTd.querySelector('.td___modalName').innerText;
                    document.getElementById('cargoSelected').value = nomeCargo;
                    closeSecondModal();
                })
            }
        }

        let idDepartamentoRegister = null;
        async function setDepartamentosRegister() {
            const response = await getDepartamentos();
            let allDepartamentos = response.content;
            const tableModal = document.querySelector('.tbody__modalDepartamento');
            allDepartamentos.map((departamento) => {
                const tr = `
                    <tr class="trDepartamento">
                        <td class="td___modalName">${departamento.nome}</td>
                        <td class="td__modalId" style="display:none">${departamento.id_departamento}</td>
                    </tr>`
                tableModal.innerHTML += tr;
            })

            const departamentoModalSelect = tableModal.getElementsByClassName('trDepartamento');
            for (let departamentoTd of departamentoModalSelect) {
                departamentoTd.addEventListener('click', async () => {
                    idDepartamentoRegister = departamentoTd.querySelector('.td__modalId').innerText;
                    const nomeDepartamento = departamentoTd.querySelector('.td___modalName').innerText;
                    document.getElementById('departamentoSelected').value = nomeDepartamento;
                    closeSecondModal();
                })
            }
        }

        let idGestorRegister = null;
        async function setGestorRegister() {
            const response = await get();
            let allColaboradores = response.content;
            const tableModal = document.querySelector('.tbody__modalGestorRegister');
            allColaboradores.map((gestor) => {
                const tr = `
                    <tr class="trGestor">
                        <td class="td__modalId">${gestor.id_colaborador}</td>
                        <td class="td___modalName">${gestor.nome}</td>
                    </tr>`
                tableModal.innerHTML += tr;
            })

            const gestorModalSelect = tableModal.getElementsByClassName('trGestor');
            for (let gestorTd of gestorModalSelect) {
                gestorTd.addEventListener('click', async () => {
                    idGestorRegister = gestorTd.querySelector('.td__modalId').innerText;
                    const nomeGestor = gestorTd.querySelector('.td___modalName').innerText;
                    document.getElementById('gestorSelected').value = nomeGestor;
                    closeSecondModal();
                })
            }
        }

        function closeSecondModal() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'none';
        }

        async function include() {

            let cpf = document.getElementById('cpf').value;
            let rg = document.getElementById('rg').value;
            let ctps = document.getElementById('ctps').value;
            let data_nascimento = document.getElementById('data_nascimento').value;
            let data_contratacao = document.getElementById('data_contratacao').value;
            let nome = document.getElementById('nome').value;
            let salario = Number(document.getElementById('salario').value);

            const params = {
                nome,
                cpf,
                rg,
                ctps,
                data_contratacao,
                data_nascimento,
                salario,
                id_cargo: Number(idCargoRegister),
                id_departamento: Number(idDepartamentoRegister),
                id_gestor: Number(idGestorRegister),
            }
            await create(params);
            return;
        }

        let items = {};
        let perPage = 6;
        const state = {
            page: 1,
            perPage,
            totalPage: Math.ceil(items.length || 0 / perPage),
            maxVisibleButtons: 5,
        }

        const list = {
            update() {
                let showItems = checkItems(items);
                document.getElementById('list').innerHTML = "";
                let page = state.page - 1;
                let start = page * state.perPage;
                let end = start + state.perPage;
                const paginatedItems = showItems.slice(start, end);
                paginatedItems.forEach(createCard);
                cardButtonsListeners();
            }
        }

        /*
        * Controlador de paginação
        */
        const controls = {
            next() {
                state.page++;
                const lastPage = state.page > state.totalPage;
                if(lastPage) {
                    state.page--;
                }
            },
            prev() {
                state.page--;
                if(state.page < 1) {
                    state.page++;
                }
            },
            goTo(page) {
                state.page = +page;
                if(page > state.totalPage) {
                    state.page = state.totalPage;
                }
                if(page < 1) {
                    state.page = 1
                }
            },
            createListeners() {
                document.querySelector('.first').addEventListener('click', () => {
                    controls.goTo(1);
                    update();
                })
                document.querySelector('.last').addEventListener('click', () => {
                    controls.goTo(state.totalPage);
                    update();
                })
                document.querySelector('.next').addEventListener('click', () => {
                    controls.next();
                    update();
                })
                document.querySelector('.prev').addEventListener('click', () => {
                    controls.prev();
                    update();
                })
            }
        }
        controls.createListeners();

        /*
        * Controlador de botões da paginação
        */
        const controllerButtons = {
            create(page) {
                const button = document.createElement('div');
                button.innerHTML = page;
                if (state.page == page) {
                    button.classList.add('active');
                }
                button.addEventListener('click', (event) => {
                    const page = event.target.innerText;
                    controls.goTo(page);
                    update();
                })
                document.querySelector('.numbers').appendChild(button);
            },
            update() {
                document.querySelector('.numbers').innerHTML = "";
                const { maxLeft, maxRight} = controllerButtons.calculateMaxButtons();
                for (let page = maxLeft; page <= maxRight; page++) {
                    controllerButtons.create(page);
                }
            },
            calculateMaxButtons() {
                const { maxVisibleButtons } = state;
                let maxLeft = (state.page) - Math.floor(maxVisibleButtons / 2);
                let maxRight = (state.page) + Math.floor(maxVisibleButtons / 2);

                if (maxLeft < 1) {
                    maxLeft = 1;
                    maxRight = maxVisibleButtons;
                }
                if (maxRight > state.totalPage) {
                    maxLeft = state.totalPage - (maxVisibleButtons - 1);
                    maxRight = state.totalPage;

                    if (maxLeft < 1) {
                        maxLeft = 1;
                    }
                }
                return { maxLeft, maxRight };
            }
        }
                
        function update() {
            list.update();
            controllerButtons.update();
        }
    }
}