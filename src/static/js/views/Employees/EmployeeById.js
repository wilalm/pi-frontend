import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";
export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Histórico");
    }

    async getHtml() {
        return `
            <h1>Histórico</h1>
            <nav class="nav__header">
            <div class="search__filter">
                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                <i class="fas fa-filter"></i>
            </div>
            </nav>
            <p class="tittle__colaborador"></p>
            <div class="table__avaliacoes">
            <table id="avaliacoes" class="order-table table display" style="width:100%">
                <thead>
                    <tr>                        
                        <th>Data</th>
                        <th>Tipo Evento</th>
                        <th>Descrição</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        `;
    }

    init() {
        const LightTableFilter = (function(Arr) {

            let input = '';
            function onInputEvent(e) {
                input = e.target;
                let tables = document.getElementsByClassName(input.getAttribute('data-table'));
                Arr.forEach.call(tables, function(table) {
                    Arr.forEach.call(table.tBodies, function(tbody) {
                        Arr.forEach.call(tbody.rows, filter);
                    });
                });
            }
    
            function filter(row) {
                let text = row.textContent.toLowerCase(), val = input.value.toLowerCase();
                row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
            }
    
            return {
                init: function() {
                    const inputs = document.getElementsByClassName('light-table-filter');
                    Arr.forEach.call(inputs, function(input) {
                        input.oninput = onInputEvent;
                    });
                }
            };
        })(Array.prototype);
        LightTableFilter.init();

        const colaborador = JSON.parse(localStorage.getItem("colaboradorHistorico"));
        document.querySelector('.tittle__colaborador').innerHTML = `<b>Colaborador: ${colaborador.nome}</b>`;

        getColaborador();

        async function getColaborador() {
            const employee = await get();
            if(employee.eventos.length > 0) {
                updateTable(employee.eventos);
            } else {
                document.querySelector('.tittle__colaborador').innerHTML = '';
                document.querySelector('tbody').innerHTML = '';
                toastr.warningSearch('Este colaborador ainda não possui histórico');
            }
        }

        function updateTable(events) {
            if(events.length < 1) {
                const body = document.querySelector('tbody');
                body.innerHTML = '';
            } else {
                const body = document.querySelector('tbody');
                body.innerHTML = '';
                events.map((event) => {

                    const tr = `
                    <tr>
                        <td>${event.data}</td>
                        <td>${event.tipo_evento}</td>
                        <td>${event.descricao}</td>
                    </tr>`
                    body.innerHTML += tr;
                })
            }
        }

        const toastr = {
            warningSearch(message) {
                const toastrModal = document.querySelector(".toastr");
                toastrModal.classList.add('show__toastr');
                const toastrmessage = toastrModal.querySelector(".toastr__message");
                toastrmessage.classList.add('toastr__warning');
                toastrmessage.innerHTML = `<p><b>Alerta</b></p>
                                            <p>${message}</p>`;
                setTimeout(function(){ 
                    toastrModal.classList.remove('show__toastr');                
                    toastrmessage.classList.remove('toastr__warning');
 
                }, 3700);
            },
        };

        async function get() {
            const data = await genericApi(`colaboradores/${colaborador.id}`, 'GET')
            return data;
        };
    }
}
