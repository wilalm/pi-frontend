import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Cargos");
    }

    async getHtml() {
        return `
            <h1>Cargos</h1>
            <nav class="nav__header">
                <div class="search__wrapper">
                    <div class="search__box">
                        <div class="search__dropdown">
                            <div class="search__default__option">Todos</div>  
                            <ul>
                                <li value="Todos">Todos</li>
                                <li value="Recent">Ativos</li>
                                <li value="Popular">Inativos</li>
                            </ul>
                        </div>
                    <div class="search__field">
                        <input type="text" class="search__input" placeholder="Pesquisar">
                        <i class="fas fa-search"></i>
                    </div>
                    </div>
                </div>
                <div class="paginate__wrapper">
                    <button class="button insert__button new">Cadastrar</button>
                    <div class="controls">
                        <div class="first">&#171;</div>
                        <div class="prev"><</div>
                        <div class="numbers">
                            <div>1</div>
                        </div>
                        <div class="next">></div>
                        <div class="last">&#187;</div>
                    </div>
                </div>
            </nav>
            <div id="list" class="list"><div>
        `;
    }

    // ***
    // INICIALIZA O SCRIPT APOS O HTML CARREGAR
    // ***
    init() {
        listItems();

        async function get() {
            const data = await genericApi(`cargos/?size=200`, 'GET')
            return data;
        }

        async function deleteItem(id) {
            const data = await genericApi(`cargos/${id}`, 'DELETE')
        }

        async function create(params) {
            const data = await genericApi(`cargos`, 'POST', params)
        }

        async function edit(id, params) {
            const data = await genericApi(`cargos/${id}`, 'PUT', params)
        }

        async function listItems() {
            const data = await get();
            items = data.content;
            state.totalPage = Math.ceil(items.length / state.perPage);
            if (items.length === 0) {
                let card = document.createElement("div");
                card.classList.add('cardList');
                const text = `<p>Nenhum Cargo Cadastrado</p>`;
                card.innerHTML = text;
                document.querySelector('#list').append(card);
            } else {
                list.update();
                controllerButtons.update();
            }
        }

        /*
        * Cria um card
        * @item - Dados do benefício para popular o card
        */
        function createCard(item) {
            let card = document.createElement("div");
            card.classList.add('cardList');
            card.id = item.id_cargo;
            const text = `<p class="card__tittle">${item.nome}</p>
                            <form class="inputs__wrapper">
                                <div>
                                    <label class="input-field">
                                        <p>Salário Minímo</p>
                                        <input name="salario_min_recomendado" value="${item.salario_min_recomendado.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}" type="text" disabled/>
                                    </label>
                                    <label class="input-field">
                                        <p>Salário Máximo</p>
                                        <input name="salario_max_recomendado" value="${item.salario_max_recomendado.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}" type="text" disabled/>
                                    </label>
                                </div>
                                <label class="input-field">
                                    <p>Descrição</p>
                                    <textarea name="Descricao" rows="5" cols="33" disabled>${item.descricao}</textarea>
                                </label>
                            </form>`
            const buttons = `<div class="card__buttons">
                                <button class="button benefits__button"><a href="/cargos/${card.id}" data-link>Benefícios</a></button>
                                <button class="button edit__button"><i class="fas fa-edit"></i>Editar</button>
                                <button class="button delete__button"><i class="far fa-trash-alt"></i>Deletar</button>
                            </div>`
            card.innerHTML = text + buttons;
            document.getElementById('list').appendChild(card);
        }

        function setCargo(id, tittle) {
            const cargo = {
                id: id,
                nome: tittle,
            };
            localStorage.setItem("cargosBeneficios", JSON.stringify(cargo))
        }

        /*
        * Observa a ação de click dos botões do card
        */
        function cardButtonsListeners() {
            const cards = document.querySelectorAll('.cardList');
            cards.forEach((card) => {
                const id = card.id;
                const tittle = card.querySelector('.card__tittle').innerText;


                const deleteButton = card.querySelector('.delete__button');
                const editButton = card.querySelector('.edit__button');
                const benefitsButton = card.querySelector('.benefits__button')

                benefitsButton.addEventListener('click', () => setCargo(card.id, tittle));
                deleteButton.addEventListener('click', () => openDeleteModal(id, tittle));
                editButton.addEventListener('click', () => editCard(card));
            })
        };

        function editCard(card) {
            const form = card.querySelector('form');

            const inputs = card.querySelectorAll('input');
            card.querySelector('textarea').disabled = false;
            inputs.forEach((input) => {
                input.disabled = false;
            })

            const cardButtons = card.querySelector('.card__buttons');
            cardButtons.style.justifyContent = 'space-between';
            const buttons = `
                            <button class="button cancel">Cancelar</button>
                            <button class="button save">Salvar</button>
                            `
            cardButtons.innerHTML = buttons;
            
            card.querySelector('.cancel').addEventListener('click', () => closeEdit(card, form));
            card.querySelector('.save').addEventListener('click', () => saveCard(card, form));
        };

        async function saveCard(card, form) {
            const id = card.id;
            let nome = card.querySelector('.card__tittle').innerText;
            let salarioMin = form.querySelector('input[name="salario_min_recomendado"]').value.replace(/[^0-9\,.-]+/g,"");
            let salarioMax = form.querySelector('input[name="salario_max_recomendado"]').value.replace(/[^0-9\,.-]+/g,"");
            const descricao = form.querySelector('textarea[name="Descricao"').value;

            salarioMin = Number.parseFloat(salarioMin);
            salarioMax = Number.parseFloat(salarioMax);

            const params = {
                nome,
                descricao,
                salarioMin,
                salarioMax
            }

            await edit(id, params);
            listItems();
            closeEdit(card);
        }

        function closeEdit(card, form) {
            card.querySelector('textarea').disabled = true;
            const inputs = card.querySelectorAll('input');
            inputs.forEach((input) => {
                input.disabled = true;
            })

            const cardButtons = card.querySelector('.card__buttons');
            cardButtons.style.justifyContent = 'flex-end';
            const buttons = `
                            <button class="button benefits__button"><a href="/cargos/${card.id}" data-link>Benefícios</a></button>
                            <button class="button edit__button"><i class="fas fa-edit"></i>Editar</button>
                            <button class="button delete__button"><i class="far fa-trash-alt"></i>Deletar</button>
                            `
            cardButtons.innerHTML = buttons;
            cardButtonsListeners();
            if(form) {
                card.querySelector('form').innerHTML = form.innerHTML;
            }
        }

        /*
        * Cria modal de confirmação antes de deletar
        */
        function openDeleteModal(id, tittle) {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal">
                                <i class="fas fa-times-circle delete__icon"></i>
                                <p class="deleteTittle">Tem certeza?</p>
                                <p>Realmente deseja deletar <b>${tittle}</b>?</p>
                                <b>Este processo não pode ser desfeito</b>
                                <div class="delete__buttons">
                                    <button class="button cancel">Cancelar</button>
                                    <button class="button delete">Deletar</button>
                                </div>
                            <div>`;
            modal.innerHTML = content;
            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.delete').addEventListener('click', async () => {
                await deleteItem(id)
                listItems();
                modal.style.display = 'none'
            });
        }

        /*
        * Search Input
        */
        const searchDropdown = document.querySelector(".search__default__option")
        const searchDropdowmList = document.querySelector(".search__dropdown ul")
        const searchDropdownItem = document.querySelectorAll(".search__dropdown ul li");
        searchDropdown.addEventListener('click', () => searchDropdowmList.classList.toggle('active'));
        searchDropdownItem.forEach((item) => {
            item.addEventListener('click', function (event) {
                const text = this.innerHTML;
                searchDropdown.innerHTML = text;
                searchDropdowmList.classList.toggle('active');
            })
        })

        const pesquisar = document.querySelector('.fa-search');
        pesquisar.addEventListener(('click'), () => list());

        const cadastrar = document.querySelector('.new');
        cadastrar.addEventListener(('click'), () => openIncludeModal());

        /*
        * Cria modal de confirmação antes de deletar
        */
        async function openIncludeModal() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal__include">
                                <b>Cadastro de Cargo</b>
                                <form class="register__form">
                                    <fieldset>
                                        <label for="nome">Nome do Cargo</label>
                                        <input type="text" name="nome" id="nome" placeholder="Insira o nome">
                                    </fieldset>
                                    <fieldset>
                                        <label for="salario_min_recomendado">Salário Mínimo</label>
                                        <input type="text" name="salario_min_recomendado" id="salario_min_recomendado" placeholder="Insira o salário minímo recomendado">
                                    </fieldset>
                                    <fieldset>
                                        <label for="salario_max_recomendado">Salário Máximo</label>
                                        <input type="text" name="salario_max_recomendado" id="salario_max_recomendado" placeholder="Insira o salário máximo recomendado">
                                    </fieldset>
                                    <fieldset>
                                        <label for="descricao">Descricao</label>
                                        <textarea rows="5" cols="33" id="descricao"></textarea>
                                    </fieldset>
                                </form>
                                <div class="include__buttons">
                                    <button class="button include">Cadastrar</button>
                                    <button class="button cancel">Cancelar</button>
                                </div>
                            <div>`;
            modal.innerHTML = content;
            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.include').addEventListener('click', async () => {
                include().then(() => {
                    listItems();
                })
                modal.style.display = 'none';
            });
        }

        async function include() {
            let nome = document.getElementById('nome').value;
            let salario_min_recomendado = document.getElementById('salario_min_recomendado').value;
            let salario_max_recomendado = document.getElementById('salario_max_recomendado').value;
            let descricao = document.getElementById('descricao').value;

            const params = {
                salario_max_recomendado,
                salario_min_recomendado,
                descricao,
                nome,
            }
            await create(params);
            return;
        }

        let items = {};
        let perPage = 6;
        const state = {
            page: 1,
            perPage,
            totalPage: Math.ceil(items.length || 0 / perPage),
            maxVisibleButtons: 5,
        }

        const list = {
            update() {
                document.getElementById('list').innerHTML = "";
                let page = state.page - 1;
                let start = page * state.perPage;
                let end = start + state.perPage;
                const paginatedItems = items.slice(start, end);
                paginatedItems.forEach(createCard);
                cardButtonsListeners();
            }
        }

        /*
        * Controlador de paginação
        */
        const controls = {
            next() {
                state.page++;
                const lastPage = state.page > state.totalPage;
                if(lastPage) {
                    state.page--;
                }
            },
            prev() {
                state.page--;
                if(state.page < 1) {
                    state.page++;
                }
            },
            goTo(page) {
                state.page = +page;
                if(page > state.totalPage) {
                    state.page = state.totalPage;
                }
                if(page < 1) {
                    state.page = 1
                }
            },
            createListeners() {
                document.querySelector('.first').addEventListener('click', () => {
                    controls.goTo(1);
                    update();
                })
                document.querySelector('.last').addEventListener('click', () => {
                    controls.goTo(state.totalPage);
                    update();
                })
                document.querySelector('.next').addEventListener('click', () => {
                    controls.next();
                    update();
                })
                document.querySelector('.prev').addEventListener('click', () => {
                    controls.prev();
                    update();
                })
            }
        }
        controls.createListeners();

        /*
        * Controlador de botões da paginação
        */
        const controllerButtons = {
            create(page) {
                const button = document.createElement('div');
                button.innerHTML = page;
                if (state.page == page) {
                    button.classList.add('active');
                }
                button.addEventListener('click', (event) => {
                    const page = event.target.innerText;
                    controls.goTo(page);
                    update();
                })
                document.querySelector('.numbers').appendChild(button);
            },
            update() {
                document.querySelector('.numbers').innerHTML = "";
                const { maxLeft, maxRight} = controllerButtons.calculateMaxButtons();
                for (let page = maxLeft; page <= maxRight; page++) {
                    controllerButtons.create(page);
                }
            },
            calculateMaxButtons() {
                const { maxVisibleButtons } = state;
                let maxLeft = (state.page) - Math.floor(maxVisibleButtons / 2);
                let maxRight = (state.page) + Math.floor(maxVisibleButtons / 2);

                if (maxLeft < 1) {
                    maxLeft = 1;
                    maxRight = maxVisibleButtons;
                }
                if (maxRight > state.totalPage) {
                    maxLeft = state.totalPage - (maxVisibleButtons - 1);
                    maxRight = state.totalPage;

                    if (maxLeft < 1) {
                        maxLeft = 1;
                    }
                }
                return { maxLeft, maxRight };
            }
        }
                
        function update() {
            list.update();
            controllerButtons.update();
        }
    }
}