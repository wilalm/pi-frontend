import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";
import Benefits from "../Benefits/Benefits.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH");
    }

    async getHtml() {
        return `
            <h1>Benefícios</h1>
            <nav class="nav__header">
                <div class="search__wrapper">
                    <div class="search__box">
                        <div class="search__dropdown">
                            <div class="search__default__option">Todos</div>  
                            <ul>
                                <li value="Todos">Todos</li>
                                <li value="Recent">Ativos</li>
                                <li value="Popular">Inativos</li>
                            </ul>
                        </div>
                    <div class="search__field">
                        <input type="text" class="search__input" placeholder="Pesquisar">
                        <i class="fas fa-search"></i>
                    </div>
                    </div>
                </div>
                <div class="paginate__wrapper">
                    <button class="button insert__button new">Associar</button>
                    <div class="controls">
                        <div class="first">&#171;</div>
                        <div class="prev"><</div>
                        <div class="numbers">
                            <div>1</div>
                        </div>
                        <div class="next">></div>
                        <div class="last">&#187;</div>
                    </div>
                </div>
            </nav>
            <b><p class="tittle__cargo"></p></b>
            <div id="list" class="list"><div>
        `;
    }

    // ***
    // INICIALIZA O SCRIPT APOS O HTML CARREGAR
    // ***
    init() {
        const cargo = JSON.parse(localStorage.getItem("cargosBeneficios"));
        listBenefits();           

        async function getBenefits() {
            const data = await genericApi(`cargos/${cargo.id}/beneficios/?=200`, 'GET')
            return data;
        }

        async function getAllBenefits() {
            const data = await genericApi(`beneficios/?=200`, 'GET')
            return data;
        }

        async function deleteBenefit(idCargo, idBeneficio) {
            const data = await genericApi(`/cargos/${idCargo}/beneficios/${idBeneficio}`, 'DELETE')
        }

        async function associateBenefit(idBeneficio) {
            const params = {};
            const data = await genericApi(`cargos/${cargo.id}/beneficios/${idBeneficio}`, 'PUT', params)
        }

        async function listBenefits() {
            const data = await getBenefits();
            benefits = data;
            state.totalPage = Math.ceil(benefits.length / state.perPage);
            if (benefits.length === 0) {
                let card = document.createElement("div");
                card.classList.add('cardList');
                const text = `<p>Nenhum Benefício Associado</p>`;
                card.innerHTML = text;
                document.querySelector('#list').append(card);
            } else {
                list.update();
                controllerButtons.update();
            }
        }

        document.querySelector('.tittle__cargo').innerHTML = `Cargo: ${cargo.nome}`;

        /*
        * Cria um card
        * @benefit - Dados do benefício para popular o card
        */
        function createCard(benefit) {
            let card = document.createElement("div");
            card.classList.add('cardList');
            card.id = benefit.id_beneficio;
            const text = `<p class="card__tittle">${benefit.nome}</p>
                            <form class="inputs__wrapper">
                                <div>
                                    <label class="input-field">
                                        <p>Valor</p>
                                        <input name="Valor" value="${benefit.valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}" type="text" disabled/>
                                    </label>
                                    <label class="input-field">
                                        <p>Coparticipação</p>
                                        <input name="Cooparticipacao" value="${benefit.coparticipacao || 0}" type="text" disabled/>
                                    </label>
                                </div>
                                <label class="input-field">
                                    <p>Descrição</p>
                                    <textarea name="Descricao" rows="5" cols="33" disabled>${benefit.descricao}</textarea>
                                </label>
                            </form>`
            const buttons = `<div class="card__buttons">
                                <button class="button delete__button"><i class="far fa-trash-alt"></i>Remover</button>
                            </div>`
            card.innerHTML = text + buttons;
            document.getElementById('list').appendChild(card);
        }

        /*
        * Observa a ação de click dos botões do card
        */
        function cardButtonsListeners() {
            const cards = document.querySelectorAll('.cardList');
            cards.forEach((card) => {
                const id = card.id;
                const tittle = card.querySelector('.card__tittle').innerText;

                const deleteButton = card.querySelector('.delete__button');
                
                deleteButton.addEventListener('click', () => openDeleteModal(id, tittle));
            })
        };

        /*
        * Cria modal de confirmação antes de deletar
        */
        function openDeleteModal(idBeneficio, tittle) {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal">
                                <i class="fas fa-times-circle delete__icon"></i>
                                <p class="deleteTittle">Tem certeza?</p>
                                <p>Realmente deseja remover <b>${tittle}</b>?</p>
                                <b>Este processo não pode ser desfeito</b>
                                <div class="delete__buttons">
                                    <button class="button cancel">Cancelar</button>
                                    <button class="button delete">Remover</button>
                                </div>
                            <div>`;
            modal.innerHTML = content;
            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.delete').addEventListener('click', async () => {
                await deleteBenefit(cargo.id, idBeneficio)
                listBenefits();
                modal.style.display = 'none'
            });
        }

        /*
        * Search Input
        */
        const searchDropdown = document.querySelector(".search__default__option")
        const searchDropdowmList = document.querySelector(".search__dropdown ul")
        const searchDropdownItem = document.querySelectorAll(".search__dropdown ul li");
        searchDropdown.addEventListener('click', () => searchDropdowmList.classList.toggle('active'));
        searchDropdownItem.forEach((item) => {
            item.addEventListener('click', function (event) {
                const text = this.innerHTML;
                searchDropdown.innerHTML = text;
                searchDropdowmList.classList.toggle('active');
            })
        })

        const pesquisar = document.querySelector('.fa-search');
        pesquisar.addEventListener(('click'), () => listBenefits());

        const cadastrar = document.querySelector('.new');
        cadastrar.addEventListener(('click'), () => openIncludeModal());

        /*
        * Cria modal de confirmação antes de deletar
        */
        async function openIncludeModal() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                <div class="modalHeader">
                                    <h4>Beneficios</h4>
                                    <div class="modalOptions">
                                        <div class="search__filter">
                                            <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                            <i class="fas fa-filter"></i>
                                        </div>
                                        <button class="button cancel">Cancelar</button>
                                    </div>
                                    <p>Selecione um Beneficio para associar</p>
                                </div>
                                <div class="modal__content">
                                    <table id="tableModalCargos" class="order-table table display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Valor</th>
                                                <th style="display: none;">Id</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody__modal">
                                        </tbody>
                                    </table>
                                </div>
                            <div>`;
            modal.innerHTML = content;
            await getBenefitsTable();
            benefitsListener();

            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
        }

        async function benefitsListener() {
            const bodyTableModal = document.querySelector('.tbody__modal');
            const cargosModalSelect = bodyTableModal.getElementsByClassName('trCargo');
            for(let benefitTd of cargosModalSelect) {
                benefitTd.addEventListener('click', async () => {
                    const idBeneficio = benefitTd.querySelector('.td__modalId').innerText;
                    await associateBenefit(idBeneficio);
                    closeModal();
                    listBenefits();
                })
            }
        }

        async function getBenefitsTable() {
            const response = await getAllBenefits();
            let allBenefits = response.content;

            let benefitsToAssociate = [];
            allBenefits.map((item) => {
                const find = benefits.find((filtro) => filtro.id_beneficio === item.id_beneficio)
                if (!find) benefitsToAssociate.push(item);
            });

            const tableModal = document.querySelector('.tbody__modal');
            benefitsToAssociate.map((benefit) => {
                const tr = `
                <tr class="trCargo">
                    <td class="td___modalName">${benefit.nome}</td>
                    <td class="td__modalValor">${benefit.valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}</td>
                    <td class="td__modalId" style="display: none;">${benefit.id_beneficio}</td>
                </tr>`
                tableModal.innerHTML += tr;
            })
        }

        function closeModal() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'none';
        }


        let benefits = {};
        let perPage = 6;
        const state = {
            page: 1,
            perPage,
            totalPage: Math.ceil(benefits.length || 0 / perPage),
            maxVisibleButtons: 5,
        }

        const list = {
            update() {
                document.getElementById('list').innerHTML = "";
                let page = state.page - 1;
                let start = page * state.perPage;
                let end = start + state.perPage;
                const paginatedItems = benefits.slice(start, end);
                paginatedItems.forEach(createCard);
                cardButtonsListeners();
            }
        }

        /*
        * Controlador de paginação
        */
        const controls = {
            next() {
                state.page++;
                const lastPage = state.page > state.totalPage;
                if(lastPage) {
                    state.page--;
                }
            },
            prev() {
                state.page--;
                if(state.page < 1) {
                    state.page++;
                }
            },
            goTo(page) {
                state.page = +page;
                if(page > state.totalPage) {
                    state.page = state.totalPage;
                }
                if(page < 1) {
                    state.page = 1
                }
            },
            createListeners() {
                document.querySelector('.first').addEventListener('click', () => {
                    controls.goTo(1);
                    update();
                })
                document.querySelector('.last').addEventListener('click', () => {
                    controls.goTo(state.totalPage);
                    update();
                })
                document.querySelector('.next').addEventListener('click', () => {
                    controls.next();
                    update();
                })
                document.querySelector('.prev').addEventListener('click', () => {
                    controls.prev();
                    update();
                })
            }
        }
        controls.createListeners();

        /*
        * Controlador de botões da paginação
        */
        const controllerButtons = {
            create(page) {
                const button = document.createElement('div');
                button.innerHTML = page;
                if (state.page == page) {
                    button.classList.add('active');
                }
                button.addEventListener('click', (event) => {
                    const page = event.target.innerText;
                    controls.goTo(page);
                    update();
                })
                document.querySelector('.numbers').appendChild(button);
            },
            update() {
                document.querySelector('.numbers').innerHTML = "";
                const { maxLeft, maxRight} = controllerButtons.calculateMaxButtons();
                for (let page = maxLeft; page <= maxRight; page++) {
                    controllerButtons.create(page);
                }
            },
            calculateMaxButtons() {
                const { maxVisibleButtons } = state;
                let maxLeft = (state.page) - Math.floor(maxVisibleButtons / 2);
                let maxRight = (state.page) + Math.floor(maxVisibleButtons / 2);

                if (maxLeft < 1) {
                    maxLeft = 1;
                    maxRight = maxVisibleButtons;
                }
                if (maxRight > state.totalPage) {
                    maxLeft = state.totalPage - (maxVisibleButtons - 1);
                    maxRight = state.totalPage;

                    if (maxLeft < 1) {
                        maxLeft = 1;
                    }
                }
                return { maxLeft, maxRight };
            }
        }
                
        function update() {
            list.update();
            controllerButtons.update();
        }
    }
}