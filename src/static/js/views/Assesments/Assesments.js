import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";
export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Avaliações");
    }

    async getHtml() {
        return `
            <h1>Avaliações</h1>
            <nav class="nav__header">
            <div class="search__wrapper">
                <div class="search__table">
                <div class="search__field">
                    <input type="text" class="search__input" placeholder="Pesquisar por Matrícula">
                    <i class="fas fa-search"></i>
                </div>
                </div>
            </div>
            <div class="search__filter">
                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                <i class="fas fa-filter"></i>
            </div>  
            <button class="button include associar__button" id="novaAvaliacao">Nova Avaliação</button>
            </nav>
            <b><p class="tittle__colaborador"></p></b>
            <div class="table__avaliacoes">
            <table id="avaliacoes" class="order-table table display" style="width:100%">
                <thead>
                    <tr>
                        <th>Data<i class="fas fa-sort"></i></th>
                        <th>Comportamento</th>
                        <th>Técnica</th>
                        <th>Trabalho em Equipe</th>
                        <th>Gestor</th>
                        <th>Comentário</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        `;
    }

    init() {
        async function getColaboradoresGerenciados(id) {
            const data = await genericApi(`colaboradores/${id}/gerenciados`, 'GET')
            return data;
        }

        const user = JSON.parse(localStorage.getItem("user"));
        const LightTableFilter = (function (Arr) {

            let input = '';
            function onInputEvent(e) {
                input = e.target;
                let tables = document.getElementsByClassName(input.getAttribute('data-table'));
                Arr.forEach.call(tables, function (table) {
                    Arr.forEach.call(table.tBodies, function (tbody) {
                        Arr.forEach.call(tbody.rows, filter);
                    });
                });
            }

            function filter(row) {
                let text = row.textContent.toLowerCase(), val = input.value.toLowerCase();
                row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
            }

            return {
                init: function () {
                    const inputs = document.getElementsByClassName('light-table-filter');
                    Arr.forEach.call(inputs, function (input) {
                        input.oninput = onInputEvent;
                    });
                }
            };
        })(Array.prototype);
        LightTableFilter.init();


        async function openIncludeModal() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Colaboradores</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Colaborador</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalCargos" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Matrícula</th>
                                                    <th>Nome</th>
                                                    <th>Cargo Atual</th>
                                                    <th style="display: none;">Id</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modal">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            await getColaboradores();
            colaboradoresListener();

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
                idColaborador = null;
            });
        }

        let idColaborador = null;
        async function colaboradoresListener() {
            const bodyTableModal = document.querySelector('.tbody__modal');
            const cargosModalSelect = bodyTableModal.getElementsByClassName('trCargo');
            for (let colaboradorTd of cargosModalSelect) {
                colaboradorTd.addEventListener('click', async () => {
                    idColaborador = colaboradorTd.querySelector('.td__modalId').innerText;
                    const nomeColaborador = colaboradorTd.querySelector('.td___modalName').innerText;
                    document.getElementById('colaboradorAvaliar').value = nomeColaborador;
                    closeSecondModal();
                })
            }
        }

        async function getColaboradores() {
            const response = await getColaboradoresGerenciados(user.id_colaborador);
            let allColaboradores = response;
            const tableModal = document.querySelector('.tbody__modal');
            allColaboradores.map((colaborador) => {
                const tr = `
                    <tr class="trCargo">
                        <td class="td__modalId">${colaborador.id_colaborador}</td>
                        <td class="td___modalName">${colaborador.nome}</td>
                        <td class="td__modalCargo">${colaborador.cargo.nome}</td>
                    </tr>`
                tableModal.innerHTML += tr;
            })
        }

        function closeSecondModal() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'none';
        }

        const pesquisar = document.querySelector('.fa-search');
        pesquisar.addEventListener(('click'), () => searchEmployee());

        async function searchEmployee() {
            const inputValue = document.querySelector('.search__input').value;
            const response = await getEmployee();
            const employees = response.content;
            let colaborador = null;
            employees.map((employee) => {
                if (employee.id_colaborador == inputValue) {
                    colaborador = employee;
                };
            })
            if (colaborador) {
                let tittle = document.querySelector('.tittle__colaborador');
                tittle.innerHTML = `Colaborador - ${colaborador.nome}`;
                updateTable(colaborador.avaliacoes, colaborador.gestor?.nome || '');
            } else {
                document.querySelector('.tittle__colaborador').innerHTML = '';
                document.querySelector('tbody').innerHTML = '';
                toastr.warningSearch('Matrícula não encontrada');
            }
        }

        function updateTable(avaliacoes, nomeGestor) {
            if (avaliacoes.length < 1) {
                const body = document.querySelector('tbody');
                body.innerHTML = '';
                toastr.warningSearch('Este colaborador não possui nenhuma avaliação');
            } else {
                const body = document.querySelector('tbody');
                body.innerHTML = '';
                avaliacoes.map((avaliacao) => {
                    const tr = `
                    <tr>
                        <td data-label="Data">${avaliacao.data}</td>
                        <td data-label="Comportamento">${avaliacao.nota_comportamento}</td>
                        <td data-label="Técnica">${avaliacao.nota_tecnica}</td>
                        <td data-label="Trabalho em Equipe">${avaliacao.nota_trabalho_em_equipe}</td>
                        <td data-label="Gestor">${nomeGestor  || 'Nenhum'}</td>
                        <td class="media_coment" data-label="Comentario">${avaliacao.comentario}</td>
                    </tr>`
                    body.innerHTML += tr;
                })
            }
        }
        
        function tranformDate(strDate) {
            let result = '';
        
            if (strDate) {
              let parts = strDate.split('-');
              result = `${parts[2]}/${parts[1]}/${parts[0]}`;
            }
            return result;
        }

        const novaAvaliacao = document.querySelector('#novaAvaliacao');
        novaAvaliacao.addEventListener('click', () => openModalAvaliacao());


        async function openModalAvaliacao() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal__include" id="modal__include">
                                <form class="register__form">
                                    <fieldset>
                                        <label for="nome">Colaborador</label>
                                        <input type="text" name="nome" id="colaboradorAvaliar" placeholder="Selecione um colaborador" value="" disabled>
                                    </fieldset>
                                    <fieldset>
                                        <label for="comportamento">Nota Comportamento</label>
                                        <input type="text" name="comportamento" id="comportamento" placeholder="Insira a nota">
                                    </fieldset>
                                    <fieldset>
                                        <label for="tecnica">Nota Técnica</label>
                                        <input type="text" name="tecnica" id="tecnica" placeholder="Insira a nota">
                                    </fieldset>
                                    <fieldset>
                                        <label for="trabalho_em_equipe">Nota Trabalho em Equipe</label>
                                        <input type="text" name="trabalho_em_equipe" id="trabalho_em_equipe" placeholder="Insira a nota">
                                    </fieldset>
                                    <fieldset>
                                        <label for="comentario">Comentário</label>
                                        <textarea rows="5" cols="33" id="comentario"></textarea>
                                    </fieldset>
                                </form>
                                <div class="include__buttons">
                                    <button class="button include lancarAvaliacao">Lançar</button>
                                    <button class="button include associar__button new">Selecionar Colaborador</button>
                                    <button class="button cancel">Cancelar</button>
                                </div>
                            <div>`;
            modal.innerHTML = content;

            const cadastrar = document.querySelector('.new');
            cadastrar.addEventListener(('click'), () => openIncludeModal());

            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.lancarAvaliacao').addEventListener('click', async () => {
                include();
                modal.style.display = 'none';
            });
        }

        function include() {
            const comentario = document.getElementById('comentario').value;
            const nota_trabalho_em_equipe = Number(document.getElementById('trabalho_em_equipe').value);
            const nota_tecnica = Number(document.getElementById('tecnica').value);
            const nota_comportamento = Number(document.getElementById('comportamento').value);

            const params = {
                id_gestor: user.id_colaborador,
                comentario,
                nota_comportamento,
                nota_tecnica,
                nota_trabalho_em_equipe,
            }

            createAvaliacao(params);

        }

        async function createAvaliacao(params) {
            const data = await genericApi(`colaboradores/${idColaborador}/avaliacoes`, 'POST', params)
        }

        async function getEmployee() {
            const data = await genericApi(`colaboradores/?size=200`, 'GET')
            return data;
        };

        const toastr = {
            warningSearch(message) {
                const toastrModal = document.querySelector(".toastr");
                toastrModal.classList.add('show__toastr');
                const toastrmessage = toastrModal.querySelector(".toastr__message");
                toastrmessage.classList.add('toastr__warning');
                toastrmessage.innerHTML = `<p><b>Alerta</b></p>
                                            <p>${message}</p>`;
                setTimeout(function () {
                    toastrModal.classList.remove('show__toastr');
                    toastrmessage.classList.remove('toastr__warning');

                }, 3700);
            },
        };
    }
}
