import AbstractView from "../AbstractView.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Meu Perfil");
    }

    async getHtml() {
        return `
            <h1>Perfil</h1>
            <p>Voce voltou a pagina de perfil!</p>
        `;
    }
}