import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Promoções");
    }

    async getHtml() {
        return `
            <h1>Promoções</h1>
            <nav class="nav__header">
                    <i class="fas fa-undo-alt" style="display:none"></i>
                    <div class="search__wrapper">
                        <div class="search__filter">
                            <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                            <i class="fas fa-filter"></i>
                        </div>
                    </div>
                    <button class="button_promote_cargo">Promover Por Cargo</button>
                    <button class="button escolha__cargo promoverPorSalario">Promover</button>
                    </nav>
            <div class="table__colaboradores">
            <table id="tableColaboradores" class="order-table table display">
                <thead>
                    <tr>
                        <th>Seleção</th>
                        <th>Matricula</th>
                        <th>Nome</th>
                        <th>Cargo Atual</th>
                        <th>Departamento</th>
                        <th>Gestor</th>
                    </tr>
                </thead>
                <tbody class="bodyTable">
                </tbody>
            </table>
        </div>
        `;
    }

    init() {
        let cargos = '';
        let cargoSelected = { id: null, nome: null };

        const promoverPorSalario = document.querySelector('.promoverPorSalario');
        promoverPorSalario.addEventListener('click', () => openModalPromoverPorSalario())

        const promoverCargo = document.querySelector('.button_promote_cargo');
        promoverCargo.addEventListener('click', () => openModalCargo());

        setTableColaboradores();

        const LightTableFilter = (function (Arr) {

            let input = '';
            function onInputEvent(e) {
                input = e.target;
                let tables = document.getElementsByClassName(input.getAttribute('data-table'));
                Arr.forEach.call(tables, function (table) {
                    Arr.forEach.call(table.tBodies, function (tbody) {
                        Arr.forEach.call(tbody.rows, filter);
                    });
                });
            }

            function filter(row) {
                let text = row.textContent.toLowerCase(), val = input.value.toLowerCase();
                row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
            }

            return {
                init: function () {
                    const inputs = document.getElementsByClassName('light-table-filter');
                    Arr.forEach.call(inputs, function (input) {
                        input.oninput = onInputEvent;
                    });
                }
            };
        })(Array.prototype);
        LightTableFilter.init();

        async function setTableColaboradores() {
            const table = document.querySelector('.table__colaboradores');
            table.style.display = 'block';

            const body = document.querySelector('.bodyTable');
            body.innerHTML = '';

            const response = await getColaboradores();
            const colaboradores = response.content;
            if (colaboradores.length < 1) {

            } else {
                colaboradores.map((colaborador) => {
                    const tr = `
                    <tr class="trColaborador">
                        <td data-label="Seleção">
                            <input type="radio" id="${colaborador.id_colaborador}" name="selectRadio" class="selectRadio" value="${colaborador.id_colaborador}">
                        </td>
                        <td data-label="Matrícula" class="idColaborador">${colaborador.id_colaborador}</td>
                        <td data-label="Nome" class="nomeColaborador media_coment">${colaborador.nome}</td>
                        <td data-label="Cargo" class="nomeColaborador media_coment">${colaborador.cargo.nome}</td>
                        <td data-label="Departamento" class="nomeColaborador media_coment">${colaborador.departamento.nome}</td>
                        <td data-label="Gestor" class="nomeColaborador media_coment">${colaborador.gestor?.nome || ''}</td>
                    </tr>`
                    body.innerHTML += tr;
                })
            }
        }

        async function openModalPromoverPorSalario() {
            const modal = document.querySelector('.fade');
            const colaboradores = document.querySelectorAll('.trColaborador');

            let idColaborador = null;
            let nomeColaborador = null;

            colaboradores.forEach(item => {
                const select = item.querySelector('.selectRadio');
                if (select.checked) {
                    idColaborador = item.querySelector('.idColaborador').innerText
                    nomeColaborador = item.querySelector('.nomeColaborador').innerText
                }
            })

            modal.style.display = 'block';
            const content = `<div class="modal__include" id="modal__include">
                                <form class="register__form">
                                    <fieldset>
                                        <label for="nome">Colaborador</label>
                                        <input type="text" name="nome" id="colaboradorPromover" value="${nomeColaborador}" disabled>
                                    </fieldset>
                                    <fieldset>
                                        <label for="grupo">Novo Cargo</label>
                                        <input type="text" name="grupo" id="grupo" placeholder="Selecione um Cargo" disabled>
                                    </fieldset>
                                    <fieldset>
                                        <label for="salario">Salário</label>
                                        <input type="text" name="salario" id="salarioAPromover" placeholder="Insira o novo salário">
                                    </fieldset>
                                </form>
                                    <div class="include__buttons">
                                        <button class="button include cadastrarUser">Promover</button>
                                        <button class="button include associar__button newGrupo" style="margin: 0 0 5% 0">Selecionar Cargo</button>
                                        <button class="button cancel">Cancelar</button>
                                    </div>
                            <div>`;
            modal.innerHTML = content;

            const cadastrarGrupo = document.querySelector('.newGrupo');
            cadastrarGrupo.addEventListener(('click'), () => openIncludeModalCargo());

            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.cadastrarUser').addEventListener('click', async () => {
                await promoverColaboradorPorSalario(idColaborador, idCargoSalarioPromover);
                modal.style.display = 'none';
                navBack();
            });
        }

        async function openModalPromoverPorCargo() {
            const modal = document.querySelector('.fade');
            const colaboradores = document.querySelectorAll('.trColaborador');
            const colaboradoresPromove = [];

            colaboradores.forEach(item => {
                const select = item.querySelector('.selectCheckbox');
                if (select.checked) {
                    const idColaborador = item.querySelector('.idColaborador').innerText
                    colaboradoresPromove.push(idColaborador);
                }
            })

            modal.style.display = 'block';
            const content = `<div class="modal__include" id="modal__include">
                                <form class="register__form">
                                    <fieldset>
                                        <label for="grupo">Novo Cargo</label>
                                        <input type="text" name="newCargoInclude" id="newCargoInclude" placeholder="Selecione um Cargo" disabled>
                                    </fieldset>
                                    <fieldset>
                                        <label for="salario">Novo Salário</label>
                                        <input type="text" name="salario" id="salarioAPromoverCargo" placeholder="Insira o novo salário">
                                    </fieldset>
                                    <fieldset>
                                        <label for="salario">Salário Mínimo Recomendado</label>
                                        <input type="text" name="salario" id="salarioMin" placeholder="Selecione um cargo" disabled>
                                    </fieldset>
                                    <fieldset>
                                        <label for="salario">Salário Máximo Recomendado</label>
                                        <input type="text" name="salario" id="salarioMax" placeholder="Selecione um cargo" disabled>
                                    </fieldset>
                                </form>
                                    <div class="include__buttons">
                                        <button class="button include promoverByCargo">Promover</button>
                                        <button class="button include associar__button newGrupo" style="margin: 0 0 5% 0">Selecionar Cargo</button>
                                        <button class="button cancel">Cancelar</button>
                                    </div>
                            <div>`;
            modal.innerHTML = content;

            const cadastrarGrupo = document.querySelector('.newGrupo');
            cadastrarGrupo.addEventListener(('click'), () => openIncludeModalCargosPlano());

            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.promoverByCargo').addEventListener('click', async () => {
                await promoverColaboradorPorCargo();
                modal.style.display = 'none';
                navBack();
            });
        }

        async function openIncludeModalCargosPlano() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Cargos</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Cargo</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalCargosRecomendados" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Cargos Recomendados</th>
                                                    <th style="display: none">id</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modalRecomendados">
                                            </tbody>
                                        </table>
                                        <table id="tableModalCargosTodos" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Outros Cargos</th>
                                                    <th style="display: none">id</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modalTodos">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            await setCargosRecomendados();

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
            });
        }

        async function setCargosRecomendados() {
            const response = await getCargoPlan(Number(cargoSelected.id));
            let allCargos = response;
            const cargosRecomendados = allCargos.cargos_indicados;
            const cargosPossiveis = allCargos.cargos_possiveis;
            const maxSalario = allCargos.salario_maximo_recomendado_para_ocargo_atual || 0;
            const minSalario = allCargos.salario_minimo_recomendado_para_ocargo_atual || 0;
            // const minSalario = allCargos. ; //Sem salário minimo no endpoint

            const tableModalCargosRecomendados = document.querySelector('.tbody__modalRecomendados');
            if (cargosRecomendados.length > 0) {
                cargosRecomendados.map((cargo) => {
                    const tr = `
                        <tr class="trCargo">
                            <td class="td___modalName">${cargo.nome}</td>
                            <td class="td__modalId" style="display:none">${cargo.id_cargo}</td>
                        </tr>`
                    tableModalCargosRecomendados.innerHTML += tr;
                })
            }

            const tableModalCargosTodos = document.querySelector('.tbody__modalTodos');
            cargosPossiveis.map((cargo) => {
                const tr = `
                    <tr class="trCargo">
                        <td class="td___modalName">${cargo.nome}</td>
                        <td class="td__modalId" style="display:none">${cargo.id_cargo}</td>
                    </tr>`
                tableModalCargosTodos.innerHTML += tr;
            })

            cargosListenerPromoteCargo(maxSalario, minSalario);
        }

        let idCargoPromoteByPlan = null;
        function cargosListenerPromoteCargo(maxSalario, minSalario) {
            let nomeCargo = null;
            const tableModalCargosTodos = document.querySelector('.tbody__modalTodos');
            const cargosModalSelect = tableModalCargosTodos.getElementsByClassName('trCargo');
            for (let grupoTd of cargosModalSelect) {
                grupoTd.addEventListener('click', async () => {
                    idCargoPromoteByPlan = grupoTd.querySelector('.td__modalId').innerText;
                    nomeCargo = grupoTd.querySelector('.td___modalName').innerText;
                    document.getElementById('newCargoInclude').value = nomeCargo;
                    document.getElementById('salarioMin').value = minSalario || 0;
                    document.getElementById('salarioMax').value = maxSalario || 0;
                    closeSecondModal();
                })
            }

            const tableModalCargosRecomendados = document.querySelector('.tbody__modalRecomendados');
            const cargosRecomendadosModalSelect = tableModalCargosRecomendados.getElementsByClassName('trCargo');
            for (let grupoTd of cargosRecomendadosModalSelect) {
                grupoTd.addEventListener('click', async () => {
                    idCargoPromoteByPlan = grupoTd.querySelector('.td__modalId').innerText;
                    nomeCargo = grupoTd.querySelector('.td___modalName').innerText;
                    document.getElementById('newCargoInclude').value = nomeCargo;
                    document.getElementById('salarioMin').value = minSalario || 0;
                    document.getElementById('salarioMax').value = maxSalario || 0;
                    closeSecondModal();
                })
            }
        }

        async function promoverColaboradorPorSalario(idColaborador, idCargo) {
            const salario = document.getElementById('salarioAPromover').value;
            const params = {
                novo_cargo: Number(idCargo),
                novo_salario: Number(salario),
            }
            const data = await genericApi(`promocao-individual/colaborador/${idColaborador}`, 'PUT', params)
            return data;
        }

        async function promoverColaboradorPorCargo() {
            const salario = document.getElementById('salarioAPromoverCargo').value;
            const id_colaboradores = [];

            const colaboradores = document.querySelectorAll('.trColaborador');
            colaboradores.forEach(item => {
                const select = item.querySelector('.selectCheckbox');
                if (select.checked) {
                    const idColaborador = item.querySelector('.idColaborador').innerText
                    id_colaboradores.push(Number(idColaborador));
                }
            })
            const params = {
                id_colaboradores,
                novo_cargo: Number(idCargoPromoteByPlan),
                novo_salario: Number(salario),
            }
            const data = await genericApi(`promocao-coletiva/cargo`, 'PUT', params)
            return data;
        }

        async function openIncludeModalCargo() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                    <div class="modalHeader">
                                        <h4>Cargos</h4>
                                        <div class="modalOptions">
                                            <div class="search__filter">
                                                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                                <i class="fas fa-filter"></i>
                                            </div>
                                            <button class="button cancel">Cancelar</button>
                                        </div>
                                        <p>Selecione um Cargo</p>
                                    </div>
                                    <div class="modal__content">
                                        <table id="tableModalCargos" class="order-table table display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Nome</th>
                                                    <th style="display: none">id</th>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody__modal">
                                            </tbody>
                                        </table>
                                    </div>
                                <div>`;
            modal.innerHTML = content;
            await setCargosSalario();
            cargosListenerPromoteSalario();

            modal.querySelector('.cancel').addEventListener('click', () => {
                modal.style.display = 'none';
            });
        }

        let idCargoSalarioPromover = null;
        async function cargosListenerPromoteSalario() {
            const bodyTableModal = document.querySelector('.tbody__modal');
            const cargosModalSelect = bodyTableModal.getElementsByClassName('trCargo');
            for (let cargoTd of cargosModalSelect) {
                cargoTd.addEventListener('click', async () => {
                    idCargoSalarioPromover = cargoTd.querySelector('.td__modalId').innerText;
                    const nomeCargo = cargoTd.querySelector('.td___modalName').innerText;
                    document.getElementById('grupo').value = nomeCargo;
                    closeSecondModal();
                })
            }
        }

        function closeSecondModal() {
            const modal = document.querySelector('.secondFade');
            modal.style.display = 'none';
        }


        async function setCargosSalario() {
            const response = await getCargos();
            let allCargos = response.content;
            const tableModal = document.querySelector('.tbody__modal');
            allCargos.map((cargo) => {
                const tr = `
                    <tr class="trCargo">
                        <td class="td___modalName">${cargo.nome}</td>
                        <td class="td__modalId" style="display:none">${cargo.id_cargo}</td>
                    </tr>`
                tableModal.innerHTML += tr;
            })
        }

        async function openModalCargo() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                <div class="modalHeader">
                                    <h4>Cargos</h4>
                                    <div class="modalOptions">
                                        <div class="search__filter">
                                            <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                            <i class="fas fa-filter"></i>
                                        </div>
                                        <button class="button cancel">Cancelar</button>
                                    </div>
                                    <p>Selecione um Cargo</p>
                                </div>
                                <div class="modal__content">
                                    <table id="tableModalCargos" class="order-table table display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th style="display: none;">Id</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody__modal">
                                        </tbody>
                                    </table>
                                </div>
                            <div>`;
            modal.innerHTML = content;
            await getCargosTable();
            cargosListener();

            const cancel = document.querySelector('.cancel');
            cancel.addEventListener('click', () => closeModal());
        }

        async function getCargosTable() {
            const response = await getCargos();
            cargos = response.content;
            const tableModal = document.querySelector('.tbody__modal');
            cargos.map((cargo) => {
                const tr = `
                <tr class="trCargo">
                    <td class="td___modalName">${cargo.nome}</td>
                    <td class="td__modalId" style="display: none;">${cargo.id_cargo}</td>
                </tr>`
                tableModal.innerHTML += tr;
            })
        }

        async function cargosListener() {
            const bodyTableModal = document.querySelector('.tbody__modal');
            const cargosModalSelect = bodyTableModal.getElementsByClassName('trCargo');
            for (let cargoTd of cargosModalSelect) {
                cargoTd.addEventListener('click', () => {
                    cargoSelected.id = cargoTd.querySelector('.td__modalId').innerText;
                    cargoSelected.nome = cargoTd.querySelector('.td___modalName').innerText;
                    closeModal();
                    createTableCargo(cargoSelected.id);
                    atualizaNav();
                })
            }
        }

        function atualizaNav() {
            let nav = document.querySelector('.nav__header');
            const button = `<nav class="nav__header">
                                <i class="fas fa-undo-alt"></i>
                                <div class="search__wrapper">
                                    <div class="search__table">
                                        <div class="search__field">
                                            <input type="text" class="search__input" placeholder="Pesquisar por Matrícula">
                                            <i class="fas fa-search"></i>
                                        </div>
                                    </div>
                                </div>
                                <button class="button_promote_cargo">Promover Por Cargo</button>
                                <button class="button escolha__cargo promoverPorCargo">Promover</button>
                            </nav>`
            nav.innerHTML = button;

            const promoverCargo = document.querySelector('.button_promote_cargo');
            promoverCargo.addEventListener('click', () => openModalCargo());

            const promoverPorCargo = document.querySelector('.promoverPorCargo');
            promoverPorCargo.addEventListener('click', () => openModalPromoverPorCargo());

            const back = document.querySelector('.fa-undo-alt');
            back.addEventListener('click', () => navBack());
        }

        function navBack() {
            let nav = document.querySelector('.nav__header');
            const button = `<nav class="nav__header">
                                <i class="fas fa-undo-alt" style="display:none"></i>
                                <div class="search__wrapper">
                                    <div class="search__filter">
                                        <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                        <i class="fas fa-filter"></i>
                                    </div>
                                </div>
                                <button class="button_promote_cargo">Promover Por Cargo</button>
                                <button class="button escolha__cargo promoverPorSalario">Promover</button>
                            </nav>`
            nav.innerHTML = button;

            const promoverCargo = document.querySelector('.button_promote_cargo');
            promoverCargo.addEventListener('click', () => openModalCargo());

            const promoverPorSalario = document.querySelector('.promoverPorSalario');
            promoverPorSalario.addEventListener('click', () => openModalPromoverPorSalario())
            setTableColaboradores();
            LightTableFilter.init();
        }

        async function createTableCargo(idCargo) {
            const table = document.querySelector('.table__colaboradores');
            table.style.display = 'block';

            const body = document.querySelector('.bodyTable');
            body.innerHTML = '';

            const colaboradoresDoCargo = await getColaboradoresByCargo(idCargo);
            if (colaboradoresDoCargo.length < 1) {

            } else {
                colaboradoresDoCargo.map((colaborador) => {
                    const tr = `
                    <tr class="trColaborador">
                        <td data-label="Seleção">
                            <input type="checkbox" id="${colaborador.id_colaborador}" name="promover" class="selectCheckbox" value="${colaborador.id_colaborador}">
                        </td>
                        <td data-label="Matrícula" class="idColaborador">${colaborador.id_colaborador}</td>
                        <td class="media_coment" data-label="Nome">${colaborador.nome}</td>
                        <td class="media_coment" data-label="Cargo">${colaborador.cargo.nome}</td>
                        <td class="media_coment" data-label="Departamento">${colaborador.departamento.nome}</td>
                        <td class="media_coment" data-label="Gestor">${colaborador.gestor?.nome || ''}</td>
                    </tr>`
                    body.innerHTML += tr;
                })
            }
        }

        async function getColaboradoresByCargo(idCargo) {
            const colaboradoresDoCargo = [];
            const response = await getColaboradores();
            const colaboradores = response.content;
            colaboradores.map((colaborador) => {
                if (colaborador.cargo.id_cargo == idCargo) {
                    colaboradoresDoCargo.push(colaborador);
                }
            })
            return colaboradoresDoCargo;
        }

        function closeModal() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'none';
        }

        async function getCargos() {
            const data = await genericApi(`cargos/?size=200`, 'GET')
            return data;
        }

        async function getCargoPlan(idCargo) {
            const data = await genericApi(`promocao-coletiva/cargo/${idCargo}`, 'GET')
            return data;
        }

        async function getColaboradores() {
            const data = await genericApi(`colaboradores/?size=200`, 'GET')
            return data;
        }
    }
}