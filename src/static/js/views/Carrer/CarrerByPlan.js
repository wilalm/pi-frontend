import AbstractView from "../AbstractView.js";
import genericApi from "../../genericApi.js";
export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("RH - Avaliações");
    }

    async getHtml() {
        return `
            <h1>Plano de Carreira</h1>
            <nav class="nav__header">
            <div class="search__filter">
                <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                <i class="fas fa-filter"></i>
            </div>
            <button class="button include associar__button new">Associar</button>
            </nav>
            <p class="tittle__carrer"></p>
            <div class="table__avaliacoes">
            <table id="avaliacoes" class="order-table table display" style="width:100%">
                <thead>
                    <tr>
                        <th>Sequência</th>
                        <th>Nome</th>
                        <th>Salário Minímo Recomendado</th>
                        <th>Salário Máximo Recomendado</th>
                        <th>Descrição</th>
                        <th>Deletar</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        `;
    }

    init() {
        const LightTableFilter = (function(Arr) {

            let input = '';
            function onInputEvent(e) {
                input = e.target;
                let tables = document.getElementsByClassName(input.getAttribute('data-table'));
                Arr.forEach.call(tables, function(table) {
                    Arr.forEach.call(table.tBodies, function(tbody) {
                        Arr.forEach.call(tbody.rows, filter);
                    });
                });
            }
    
            function filter(row) {
                let text = row.textContent.toLowerCase(), val = input.value.toLowerCase();
                row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
            }
    
            return {
                init: function() {
                    const inputs = document.getElementsByClassName('light-table-filter');
                    Arr.forEach.call(inputs, function(input) {
                        input.oninput = onInputEvent;
                    });
                }
            };
        })(Array.prototype);
        LightTableFilter.init();

        const plano_carreira = JSON.parse(localStorage.getItem("carreiraCargo"));
        document.querySelector('.tittle__carrer').innerHTML = plano_carreira.nome;

        const cadastrar = document.querySelector('.new');
        cadastrar.addEventListener(('click'), () => openIncludeModal());

        async function openIncludeModal() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal modal__cargos">
                                <div class="modalHeader">
                                    <h4>Cargos</h4>
                                    <div class="modalOptions">
                                        <div class="search__filter">
                                            <input type="search" class="light-table-filter" data-table="order-table" placeholder="Filtro">
                                            <i class="fas fa-filter"></i>
                                        </div>
                                        <button class="button cancel">Cancelar</button>
                                    </div>
                                    <p>Selecione um Cargo para associar</p>
                                </div>
                                <div class="modal__content">
                                    <table id="tableModalCargos" class="order-table table display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th style="display: none;">Id</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody__modal">
                                        </tbody>
                                    </table>
                                </div>
                            <div>`;
            modal.innerHTML = content;
            await getCargosTable();
            cargosListener();

            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
        }

        async function cargosListener() {
            const bodyTableModal = document.querySelector('.tbody__modal');
            const cargosModalSelect = bodyTableModal.getElementsByClassName('trCargo');
            for(let cargoTd of cargosModalSelect) {
                cargoTd.addEventListener('click', async () => {
                    const idCargo = Number(cargoTd.querySelector('.td__modalId').innerText);
                    await associateCargo(idCargo);
                    closeModal();
                    getCarrerPlan();
                })
            }
        }

        function closeModal() {
            const modal = document.querySelector('.fade');
            modal.style.display = 'none';
        }


        async function getCargosTable() {
            const response = await getAllCargos();
            let allCargos = response.content;

            let cargosToAssociate = [];
            allCargos.map((item) => {
                const find = cargos.find((filtro) => filtro.cargo.id_cargo === item.id_cargo)
                if (!find) cargosToAssociate.push(item);
            });

            const tableModal = document.querySelector('.tbody__modal');
            cargosToAssociate.map((cargo) => {
                const tr = `
                <tr class="trCargo">
                    <td class="td___modalName">${cargo.nome}</td>
                    <td class="td__modalId" style="display: none;">${cargo.id_cargo}</td>
                </tr>`
                tableModal.innerHTML += tr;
            })
        }

        getCarrerPlan();

        let cargos;
        async function getCarrerPlan() {
            const response = await get();
            cargos = response;
            if(cargos) {
                cargos.sort((a, b) => a.sequencia - b.sequencia);
                updateTable(cargos);
            } else {
                document.querySelector('.tittle__colaborador').innerHTML = '';
                document.querySelector('tbody').innerHTML = '';
            }
        }
    

        function updateTable(cargos) {
            if(cargos.length < 1) {
                const body = document.querySelector('tbody');
                body.innerHTML = '';
            } else {
                const body = document.querySelector('tbody');
                body.innerHTML = '';
                cargos.map((item) => {
                    const tr = `
                    <tr class="cargoOption">
                        <td style="display:none" class="id_cargo_plan">${item.cargo.id_cargo}</td>
                        <td data-label="Sequência">${item.sequencia}</td>
                        <td class="name_cargo_plan media_coment" data-label="Nome">${item.cargo.nome}</td>
                        <td data-label="Salário Min.">${item.cargo.salario_min_recomendado.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}</td>
                        <td data-label="Salário Máx.">${item.cargo.salario_max_recomendado.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}</td>
                        <td data-label="Descrição" class="media_coment">${item.cargo.descricao}</td>
                        <td data-label="Deletar" class="media_coment">
                            <button class="button deleteUser__button"><i class="fas fa-minus-circle"></i></button>
                        </td>
                    </tr>`
                    body.innerHTML += tr;
                })
                buttonsListenerTable();
            }
        }

        function buttonsListenerTable() {
            const tableRows = document.querySelectorAll('.cargoOption');
            tableRows.forEach(row => {
                const id = row.querySelector('.id_cargo_plan').innerText;
                const name = row.querySelector('.name_cargo_plan').innerText;
                row.querySelector('.deleteUser__button').addEventListener('click', () => openDeleteModal(id, name));
            });
        }

        function openDeleteModal(id, tittle) {
            const modal = document.querySelector('.fade');
            modal.style.display = 'block';
            const content = `<div class="modal">
                                        <i class="fas fa-times-circle delete__icon"></i>
                                        <p class="deleteTittle">Tem certeza?</p>
                                        <p>Realmente deseja deletar o usuário de <b>${tittle}</b>?</p>
                                        <b>Este processo não pode ser desfeito</b>
                                        <div class="delete__buttons">
                                            <button class="button cancel">Cancelar</button>
                                            <button class="button delete">Deletar</button>
                                        </div>
                                    <div>`;
            modal.innerHTML = content;
            modal.querySelector('.cancel').addEventListener('click', () => modal.style.display = 'none');
            modal.querySelector('.delete').addEventListener('click', async () => {
                await deleteCargoFromPlan(id)
                getCarrerPlan();
                modal.style.display = 'none'
            });
        }

        async function get() {
            const data = await genericApi(`plano_carreira/${plano_carreira.id}/cargos/?size=200`, 'GET')
            return data;
        };

        async function associateCargo(idCargo) {
            const sequencia = cargos[cargos.length - 1]?.sequencia || 0;
            const params = {
                id_cargo: idCargo,
                sequencia: sequencia + 1,
            };
            const data = await genericApi(`plano_carreira/${plano_carreira.id}/cargos`, 'PUT', params)
        }

        async function getAllCargos() {
            const data = await genericApi(`cargos/?=200`, 'GET')
            return data;
        }

        async function deleteCargoFromPlan(idCargo) {
            const data = await genericApi(`/plano_carreira/${plano_carreira.id}/cargos/${idCargo}`, 'DELETE')
            return data;
        }

        const toastr = {
            warningSearch(message) {
                const toastrModal = document.querySelector(".toastr");
                toastrModal.classList.add('show__toastr');
                const toastrmessage = toastrModal.querySelector(".toastr__message");
                toastrmessage.classList.add('toastr__warning');
                toastrmessage.innerHTML = `<p><b>Alerta</b></p>
                                            <p>${message}</p>`;
                setTimeout(function(){ 
                    toastrModal.classList.remove('show__toastr');                
                    toastrmessage.classList.remove('toastr__warning');
 
                }, 3700);
            },
        };
    }
}
